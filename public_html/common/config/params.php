<?php
return [
    'adminEmail' => 'admin@terlabs.com',
    'supportEmail' => 'support@terlabs.com',
    'siteName' => 'K&GO',
    'user.passwordResetTokenExpire' => 3600,
];
