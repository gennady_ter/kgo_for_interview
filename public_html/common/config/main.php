<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=admin_kgo',
            'username' => 'admin_kgo',
            'password' => 'tquuPnkZBa',
            'charset' => 'utf8',
            'enableSchemaCache' => true,
            'schemaCacheDuration' => 3600,
            'schemaCache' => 'cache',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
//        enable on new server
//        'apns' => [
//            'class' => 'bryglen\apnsgcm\Apns',
//            'environment' => \bryglen\apnsgcm\Apns::ENVIRONMENT_SANDBOX,
//            'pemFile' => dirname(__FILE__).'/apnssert/apns-dev.pem',
//            // 'retryTimes' => 3,
//            'options' => [
//                'sendRetryTimes' => 5
//            ]
//        ],
    ],
];
