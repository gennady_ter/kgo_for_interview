<?php
namespace common\components;

use common\models\User;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "driver".
 *
 * @property int $id
 * @property int $user_id
 * @property string $username
 * @property string $email
 * @property string $photo
 * @property int $status
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $address
 * @property string $firstname
 * @property string $lastname
 * @property int $rating
 *
 * @property-read User $user
 * @property-read string $fullname
 */

class UserRelated extends ActiveRecord {

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }


    public function getFullname() {
        $user = $this->user;
        if(!is_null($user)) {
            return $user->firstname . ' ' . $user->lastname;
        }
        return null;
    }

    public function getInfo($field) {
        if($this->user) {
            return $this->user->$field;
        }
        return null;
    }

    public function getUsername() {
        if($this->user) {
            return $this->user->username;
        }
        return null;
    }

    public function getPhoto() {
        if($this->user) {
            return $this->user->photo;
        }
        return null;
    }

    public function getEmail() {
        if($this->user) {
            return $this->user->email;
        }
        return null;
    }
    public function getStatus() {
        if($this->user) {
            return $this->user->status;
        }
        return null;
    }

    public function getAddress() {
        if($this->user) {
            return $this->user->address;
        }
        return null;
    }
    public function getFirstname() {
        if($this->user) {
            return $this->user->firstname;
        }
        return null;
    }

    public function getLastname() {
        if($this->user) {
            return $this->user->lastname;
        }
        return null;
    }

    public function getRating() {
        if($this->user) {
            return $this->user->rating;
        }
        return null;
    }

}