<?php
namespace common\components;

use common\models\Trip;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "driver".
 *
 * @property Trip[] $trips
 * @property ActiveQuery|Trip[] $tripsMonth
 * @property ActiveQuery|Trip[] $tripsWeek
 * @property-read int $tripsTotal
 * @property-read int $tripsMonthTotal
 * @property-read int $tripsWeekTotal
 */

trait Trips {

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'id' => 'ID',
            'trips' => 'Trips',
            'tripsMonth' => 'Trips for 30 days',
            'tripsWeek' => 'Trips for 7 days',
            'tripsTotal' => 'Total trips',
            'tripsMonthTotal' => 'Count trips for 30 days',
            'tripsWeekTotal' => 'Count trips for 7 days',
        ]);
    }

    /**
     * @return ActiveQuery|Trip[]
     */
    public function getTrips()
    {
        /** @var ActiveRecord $this */
        return $this->hasMany(Trip::className(), [$this::tableName().'_id' => 'id'])->orderBy('created_at DESC');
    }

    /**
     * @param $statuses array
     * @return array
     */
    public function getTripsByStatuses($statuses) {
        return $this->getTrips()->where(['in', 'status', $statuses])->all();
    }

    /**
     * @return ActiveQuery|Trip[]
     */
    public function getTripsMonth() {
        return $this->getTrips()->where(['>', 'finished_at',
            date('Y-m-d H:i:s', strtotime('-30 days'))
        ]);
    }

    /**
     * @return ActiveQuery|Trip[]
     */
    public function getTripsWeek() {
        return $this->getTrips()->where(['>', 'finished_at',
            date('Y-m-d H:i:s', strtotime('-7 days'))
        ]);
    }

    /**
     * @return  int
     */
    public function getTripsTotal()
    {
        return count($this->trips);
    }

    /**
     * @return  int
     */
    public function getTripsMonthTotal()
    {
        return count($this->tripsMonth);
    }

    /**
     * @return  int
     */
    public function getTripsWeekTotal()
    {
        return count($this->tripsWeek);
    }


    public function getRelatedTrips($model, $attribute) {
        $trips = [];
        foreach (ArrayHelper::getColumn($this->$model, $attribute) as $item) {
            $trips = array_merge($trips, $item);
        }
        return $trips;
    }



}