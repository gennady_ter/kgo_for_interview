<?php
namespace common\components;

class Request extends \yii\web\Request
{
	public $web;
	public $appUrl;

	public function getBaseUrl()
	{
		return str_replace($this->web, "", parent::getBaseUrl()) . $this->appUrl;
	}

	public function resolvePathInfo()
	{
		if ($this->getUrl() === $this->appUrl) {
			return "";
		} else {
			return parent::resolvePathInfo();
		}
	}
}
?>