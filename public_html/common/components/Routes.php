<?php
namespace common\components;

use common\models\Route;
use common\models\Trip;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "driver".
 *
 * @property Route[] $routes
 */

trait Routes {

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'id' => 'ID',
            'routes' => 'Routes'
        ]);
    }

    /**
     * @return ActiveQuery|Trip[]
     */
    public function getRoutes()
    {
        /** @var ActiveRecord $this */
        return $this->hasMany(Route::className(), [$this::tableName().'_id' => 'id'])->orderBy('created_at DESC');
    }


    public function getRelatedRoutes($model, $attribute) {
        $routes = [];
        foreach (ArrayHelper::getColumn($this->$model, $attribute) as $item) {
            $routes = array_merge($routes, $item);
        }
        return ArrayHelper::index($routes, 'id');
    }



}