<?php

namespace common\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "car".
 *
 * @property int $id
 * @property int $driver_id
 * @property string $mark
 * @property string $model
 * @property string $year
 * @property double $efficiency
 * @property int $seats
 * @property string $number
 * @property string $insurance
 * @property string $registration
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Driver $driver
 */
class Car extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'car';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['driver_id', 'mark', 'model', 'year', 'efficiency', 'seats', 'number'], 'required'],
            [['driver_id', 'seats'], 'integer'],
            [['insurance', 'registration', 'created_at', 'updated_at'], 'safe'],
            [['efficiency'], 'number'],
            [['mark', 'model', 'number'], 'string', 'max' => 255],
            [['driver_id'], 'exist', 'skipOnError' => true, 'targetClass' => Driver::className(), 'targetAttribute' => ['driver_id' => 'id']],
            [['!insurance', '!registration'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'driver_id' => 'Driver ID',
            'mark' => 'Mark',
            'model' => 'Model',
            'year' => 'Year',
            'efficiency' => 'Efficiency',
            'seats' => 'Seats',
            'number' => 'License Plate',
            'insurance' => 'Insurance',
            'registration' => 'Registration',
            'created_at' => 'Active Since',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDriver()
    {
        return $this->hasOne(Driver::className(), ['id' => 'driver_id']);
    }


    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($this->validate()) {
            /**
             * @var $file UploadedFile
             */
            $files = [
                'insurance' => UploadedFile::getInstance($this, 'insurance'),
                'registration' => UploadedFile::getInstance($this, 'registration')
            ];
            foreach ($files as $attribute => $file) {
                if($file) {
                    $file->saveAs(Yii::getAlias('@common/uploads/car/'.$attribute.'/'). $file->baseName . '.' . $file->extension);
                    $this->$attribute = '/common/uploads/car/'.$attribute.'/' . $file->baseName . '.' . $file->extension;
                } elseif (!$file && is_null($this->$attribute)) {
                    return false;
                }
            }

            return parent::beforeSave($insert);
        } else {
            return false;
        }
    }


    public function afterFind()
    {
        parent::afterFind();
        if($this->insurance) {
            $this->insurance = 'http://'.Yii::$app->getRequest()->serverName.$this->insurance;
        }
        if($this->registration) {
            $this->registration = 'http://'.Yii::$app->getRequest()->serverName.$this->registration;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if($this->insurance) {
            $this->insurance = 'http://'.Yii::$app->getRequest()->serverName.$this->insurance;
        }
        if($this->registration) {
            $this->registration = 'http://'.Yii::$app->getRequest()->serverName.$this->registration;
        }
    }
}
