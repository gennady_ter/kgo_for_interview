<?php

namespace common\models;

use backend\components\ApiConfigTrait;
use common\components\Trips;
use common\components\UserRelated;
use tuyakhov\jsonapi\Inflector;
use tuyakhov\jsonapi\LinksInterface;
use tuyakhov\jsonapi\ResourceInterface;
use tuyakhov\jsonapi\ResourceTrait;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Link;
use yii\web\UploadedFile;

/**
 * This is the model class for table "driver".
 *
 * @property int $id
 * @property int $user_id
 * @property string $description
 * @property string $birthdate
 * @property string $ethnicity
 * @property int $sex
 * @property int $family_status
 * @property string $education
 * @property string $current_job
 * @property string $experience
 * @property string $resume
 * @property int $is_experienced_mom
 * @property string $drivers_license
 * @property string $expiration_date
 * @property string $available_from
 * @property string $available_to
 * @property float $balance
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Car[] $cars
 * @property Car $car
 * @property Route[] $routes
 * @property Child[] $children
 * @property User $user
 * @property JobHistory[] $jobHistories
 * @property Recommendation[] $recommendations
 * @property Review[] $reviews
 * @property Interview[] $interviews
 * @property string $lastActivity
 */
class Driver extends UserRelated
{
    use Trips;
    use ApiConfigTrait;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'driver';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'description', 'birthdate', 'education', 'current_job', 'experience', 'available_from', 'available_to'], 'required'],
            [['ethnicity', 'user_id', 'sex', 'family_status'], 'integer'],
            [['description', 'education'], 'string'],
            [['balance', 'drivers_license', 'resume', 'created_at', 'updated_at', 'ethnicity', 'sex', 'family_status'], 'safe'],
            [['birthdate', 'expiration_date'], 'date', 'format' => 'yyyy-M-d'],
            [['current_job', 'experience'], 'string', 'max' => 255],
            ['ethnicity', 'in', 'range' => array_keys($this->ethnicities())],
            [['is_experienced_mom'], 'boolean'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['!drivers_license', '!resume'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'description' => 'Description',
            'birthdate' => 'Birthdate',
            'ethnicity' => 'Ethnicity',
            'sex' => 'Sex',
            'family_status' => 'Family Status',
            'education' => 'Education',
            'current_job' => 'Current Job',
            'experience' => 'Experience',
            'resume' => 'Resume',
            'is_experienced_mom' => 'Is Experienced Mom',
            'drivers_license' => 'Drivers License',
            'expiration_date' => 'DL Expiration',
            'available_from' => 'Available Time From',
            'available_to' => 'Available Time To',
            'balance' => 'Balance',
            'created_at' => 'Active Since',
            'updated_at' => 'Updated At',
            'lastActivity' => 'Last Activity'
        ];
    }

    public function ethnicities() {
        return [
            'White / Caucasian',
            'Hispanic / Latino',
            'Black / African American',
            'Asian',
            'American Indian or Alaska Native',
            'Native Hawaiian or Other Pacific Islander',
            'Prefer not to say',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCars()
    {
        return $this->hasMany(Car::className(), ['driver_id' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCar()
    {
        return $this->hasOne(Car::className(), ['driver_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChildren()
    {
        return $this->hasMany(Child::className(), ['driver_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoutes()
    {
        return $this->hasMany(Route::className(), ['driver_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobHistories()
    {
        return $this->hasMany(JobHistory::className(), ['driver_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecommendations()
    {
        return $this->hasMany(Recommendation::className(), ['driver_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviews()
    {
        return $this->hasMany(Review::className(), ['driver_id' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInterviews()
    {
        return $this->hasMany(Interview::className(), ['driver_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInterview()
    {
        return $this->hasOne(Interview::className(), ['id' => 'driver_id']);
    }


    public function sexValues() {
        return ['Man', 'Woman', 'Prefer not to say'];
    }

    public function sexName() {
        return $this->sexValues()[$this->sex];
    }


    public function familyStatuses() {
        return ['Married', 'Divorced', 'Alone', 'Other'];
    }

    public function familyStatusName() {
        return $this->familyStatuses()[$this->family_status];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($this->validate()) {
            /**
             * @var $file UploadedFile
             */
            $files = [
                'resume' => UploadedFile::getInstance($this, 'resume'),
                'drivers_license' => UploadedFile::getInstance($this, 'drivers_license')
            ];
            foreach ($files as $attribute => $file) {
                if($file) {
                    $file->saveAs(Yii::getAlias('@common/uploads/driver/'.$attribute.'/'). $file->baseName . '.' . $file->extension);
                    $this->$attribute = '/common/uploads/driver/'.$attribute.'/' . $file->baseName . '.' . $file->extension;
                } elseif (!$file && is_null($this->$attribute)) {
                    return false;
                }
            }

            return parent::beforeSave($insert);
        } else {
            return false;
        }
    }

    public function afterDelete() {
        parent::afterDelete();
        $this->user->delete();
    }

    public function afterFind()
    {
        parent::afterFind();
        if($this->drivers_license) {
            $this->drivers_license = 'http://'.Yii::$app->getRequest()->serverName.$this->drivers_license;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if($this->drivers_license) {
            $this->drivers_license = 'http://'.Yii::$app->getRequest()->serverName.$this->drivers_license;
        }
    }


    public static function getList() {

        $results = [];
        foreach (self::find()->all() as $driver) {
            /** @var $driver self */
            $results[$driver->id] = $driver->user->firstname.' '.$driver->user->lastname;
        }

        return $results;
    }

    /**
     * @param $statuses array
     * @return array
     */
    public function getTripsByStatuses($statuses) {
        return $this->getTrips()
            ->rightJoin('route', 'trip.route_id = route.id')
            ->andWhere('route.group_id is null')
            ->orWhere('route.group_id is not null and route.is_main = 1')
            ->andWhere(['in', 'trip.status', $statuses])
            ->all();
    }

    /**
     * @return int|string
     */
    public function getTripsTotal() {
        return $this->getTrips()->count();
    }

    /**
     * @return int|string
     */
    public function getTripsMonthTotal() {
        return $this->getTripsMonth()->count();
    }
    /**
     * @return int|string
     */
    public function getTripsWeekTotal() {
        return $this->getTripsWeek()->count();
    }

    public function getLastActivity() {
        /** @var Trip $last */
        $last = $this->getTrips()->orderBy('finished_at DESC')->one();
        if($last) {
            return $last->finished_at;
        } else {
            return null;
        }
    }


    /**
     * @param $params
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function searchForApi($params) {

        if(!$params) {
            $params = Yii::$app->params['settings'];
        } else {
            foreach ($params as $i => $param) {
                unset($params[$i]);
                $params['choose_auto_'.$i] = $param;
            }
        }

//        search by rating
        $drivers = User::find()->leftJoin('driver', 'user.id = driver.user_id')->where([
            '>=',
            'rating',
            $params['choose_auto_rating']]
        )->andWhere(['type' => User::TYPE_DRIVER])->select('driver.id')->column();

//        search by count trips
        $drivers = Trip::find()->select('driver_id, count(*) as count')
            ->where(['in', 'driver_id', $drivers])
            ->groupBy('driver_id')
            ->having('count >= '.$params['choose_auto_count_trips'])
            ->column();

//        search by count location
//        $drivers = User::find()->leftJoin('driver', 'user.id = driver.user_id')
//            ->where([
//                '<',
//                'address',
//                $params['choose_auto_count_trips']
//            ])->select('driver.id')->column();

//        search by availability
        $drivers = Driver::find()
            ->where(['>=', 'available_from', $params['choose_auto_available_from']])
            ->andWhere(['<=', 'available_to', $params['choose_auto_available_to']])
            ->andWhere(['in', 'id', $drivers])
            ->all();

        return $drivers;
    }
}
