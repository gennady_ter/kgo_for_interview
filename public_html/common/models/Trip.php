<?php

namespace common\models;

use backend\components\ApiConfigTrait;
use backend\components\MinutesHelper;
use tuyakhov\jsonapi\Inflector;
use tuyakhov\jsonapi\LinksInterface;
use tuyakhov\jsonapi\ResourceInterface;
use tuyakhov\jsonapi\ResourceTrait;
use Yii;
use yii\helpers\Url;
use yii\web\Link;

/**
 * This is the model class for table "trip".
 *
 * @property int $id
 * @property int $route_id
 * @property int $driver_id
 * @property int $family_member_id
 * @property double $miles
 * @property string $created_at
 * @property string $start_at
 * @property string $wait_begin_at
 * @property string $wait_finish_at
 * @property string $finished_at
 * @property int $status
 *
 * @property Invoice[] $invoices
 * @property NannyService[] $nannyServices
 * @property Route $route
 * @property Driver $driver
 * @property FamilyMember $familyMember
 * @property string $statusName
 * @property float $minutes
 * @property float $waiting
 * @property Child[] $children
 */
class Trip extends \yii\db\ActiveRecord
{
    use ApiConfigTrait;

    const STATUS_NO_DRIVER = 0;
    const STATUS_AWAITING_ALL = 1;
    const STATUS_AWAITING_FAMILY_MEMBER = 2;
    const STATUS_AWAITING_DRIVER = 3;
    const STATUS_DECLINED_FAMILY_MEMBER = 4;
    const STATUS_DECLINED_DRIVER = 5;
    const STATUS_TODO = 6;
    const STATUS_ACTIVE = 7;
    const STATUS_PAUSED = 8;
    const STATUS_DONE = 9;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'trip';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['route_id', 'family_member_id'], 'required'],
            [['route_id', 'driver_id', 'status'], 'integer'],
            [['miles'], 'number'],
            [['start_at', 'created_at', 'wait_begin_at', 'wait_finish_at', 'finished_at'], 'safe'],
            [['route_id'], 'exist', 'skipOnError' => true, 'targetClass' => Route::className(), 'targetAttribute' => ['route_id' => 'id']],
            ['status', 'in', 'range' => array_keys($this->statusLabels())]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'route_id' => 'Route ID',
            'driver_id' => 'Driver ID',
            'family_member_id' => 'Family Member ID',
            'miles' => 'Miles',
            'created_at' => 'Active Since',
            'start_at' => 'Start at',
            'wait_begin_at' => 'Wait Begin At',
            'wait_finish_at' => 'Wait Finish At',
            'finished_at' => 'Finished At',
            'status' => 'Status',
        ];
    }

    /**
     * @return array
     */
    public function statusLabels() {
        return [
            'Has not driver',
            'Waiting for confirmation of driver and family member',
            'Waiting for confirmation of family member',
            'Waiting for confirmation of driver',
            'Declined by family member',
            'Declined by driver',
            'Accepted by driver and family member',
            'Active',
            'Paused',
            'Done'
        ];
    }

    public function getData() {
        $route = $this->route;
        $data = [
            'points' => [
                $route->point_a,
                $route->point_b
            ],
            'route' => $route->getCommonValues(),
            'families' => $this->getFamilyData()
        ];
        if($route->driver) {
            $data['driver'] = [
                'id' => $route->driver->id,
                'firstname' => $route->driver->firstname,
                'lastname' => $route->driver->lastname,
                'photo' => $route->driver->photo
            ];
        }
        return $data;
    }

    protected function getFamilyData() {
        $data = [];
        $familyMember = $this->route->familyMember;
        $data['id'] = $familyMember->family_id;

        $data['familyMember']['id'] = $familyMember->id;
        $data['familyMember']['name'] = $familyMember->fullname ;
        $data['familyMember']['photo'] = $familyMember->photo;

        $route = $this->route;
        $data['children'][$route->child_id] = [
            'id' => $route->child_id,
            'name' => $route->child->name,
            'photo' => $route->child->photo,
//                'route' => $route->getUniqueValues()
        ];

        return [$data];
    }

    public static function getStatusesByGroup($group) {

        if($group == 'awaiting') {
            return [
                self::STATUS_NO_DRIVER,
                self::STATUS_AWAITING_ALL,
                self::STATUS_AWAITING_DRIVER,
                self::STATUS_AWAITING_FAMILY_MEMBER,
            ];
        } elseif($group == 'active') {
            return [
                self::STATUS_TODO,
                self::STATUS_ACTIVE,
                self::STATUS_PAUSED
            ];
        } elseif($group == 'archived') {
            return [
                Trip::STATUS_DECLINED_DRIVER,
                Trip::STATUS_DECLINED_FAMILY_MEMBER,
                Trip::STATUS_DONE
            ];
        }

        return [];

    }

    /**
     * @return mixed
     */
    public function getStatusName() {
        return $this->statusLabels()[$this->status];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoices()
    {
        return $this->hasMany(Invoice::className(), ['trip_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNannyServices()
    {
        return $this->hasMany(NannyService::className(), ['trip_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoute()
    {
        return $this->hasOne(Route::className(), ['id' => 'route_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDriver()
    {
        return $this->hasOne(Driver::className(), ['id' => 'driver_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFamilyMember()
    {
        return $this->hasOne(FamilyMember::className(), ['id' => 'family_member_id']);
    }

    /**
     * @return float
     */
    public function getMinutes() {
        return MinutesHelper::count($this->start_at, $this->finished_at) - $this->waiting;
    }

    /**
     * @return float
     */
    public function getWaiting() {
        if($this->wait_begin_at != $this->wait_finish_at) {
            return MinutesHelper::count($this->wait_begin_at, $this->wait_finish_at);
        } else {
            return false;
        }
    }

    /**
     * @return array
     */
    public function getChildren() {
        if($this->route->group_id) {
            return $this->route->group->getChildren();
        } else {
            return [$this->route->child];
        }
    }


    /**
     * @param null $for
     * @return float|int
     */
    public function countMoney($for = 'client') {
        $settings = Yii::$app->params['settings'];

        $money = ($settings['cost_tripindividual'] * $this->minutes)
            + ($this->miles/$this->driver->car->efficiency) * $settings['gas_price'];

        if($this->waiting) {
            $money += $settings['cost_wait'] * $this->waiting;
        }

        if($this->nannyServices) {
            foreach ($this->nannyServices as $nannyService) {
                $money += $nannyService->minutes * $settings['cost_nannyservice'];
            }
        }

        if($for == 'driver') {
            $money *= 0.8;
        } elseif($for == 'service') {
            $money *= 0.2;
        } elseif($for == 'client' & $this->route->group_id) {
            // todo: add for brothers and sisters
            $money /= $this->route->object->countChildren;
        }

        return number_format($money, 2, '.', '');

    }

    protected function checkUser($type = 'full') {
        /** @var User $user */
        $user = Yii::$app->user->getIdentity();
        if($type == 'driver') {
            return $user->isDriver() && $this->driver_id == $user->driver->id;
        } elseif($type = 'familyMember') {
            return $user->isFamilyMember() && $this->family_member_id == $user->familyMember->id;
        } else {
            return $user->type == $user::TYPE_FULL;
        }

    }

    public function accept() {
        if($this->checkUser('familyMember')) {
            if($this->status == $this::STATUS_AWAITING_FAMILY_MEMBER) {
                $this->status = $this::STATUS_TODO;
            } elseif($this->status == $this::STATUS_AWAITING_ALL) {
                $this->status = $this::STATUS_AWAITING_DRIVER;
            }

        } elseif($this->checkUser('driver')) {
            if($this->status == $this::STATUS_AWAITING_DRIVER) {
                $this->status = $this::STATUS_TODO;
            } elseif($this->status == $this::STATUS_AWAITING_ALL) {
                $this->status = $this::STATUS_AWAITING_FAMILY_MEMBER;
            }
        }

        return $this->save();
    }

    public function decline() {
        if($this->checkUser('familyMember')) {
            $this->status = $this::STATUS_DECLINED_FAMILY_MEMBER;
        } elseif($this->checkUser('driver')) {
            $this->status = $this::STATUS_DECLINED_DRIVER;
        }
        if($this->save()) {
            $invoice = new Invoice([
                'type' => Invoice::TYPE_PENALTY,
                'user_id' => $this->familyMember->user_id,
                'object_id' => $this->id,
                'amount' => Yii::$app->params['settings']['penalty']
            ]);
            if(!$invoice->save()) {
                $this->addErrors($invoice->getErrors());
            }
            return true;
        } else {
            return false;
        }
    }


    public function finish() {
        if($this->status != $this::STATUS_DONE) {
            $this->status = $this::STATUS_DONE;
            $this->finished_at = date('Y-m-d H:i:s');
            if($this->save()) {
//            invoice for client
                $clientAmount = $this->countMoney('client');
                if($this->route->group_id) {
                    foreach ($this->children as $child) {
                        $familyMember = $child->family->getFirstFamilyMember();
                        $invoice = new Invoice([
                            'type' => Invoice::TYPE_TRIP,
                            'user_id' => $familyMember->user->id,
                            'object_id' => $this->id,
                            'amount' => $clientAmount
                        ]);

                        if(!$invoice->save()) {
                            return $invoice->getErrors();
                        }
                    }
                } else {
                    $invoice = new Invoice([
                        'type' => Invoice::TYPE_TRIP,
                        'user_id' => $this->familyMember->user->id,
                        'object_id' => $this->id,
                        'amount' => $clientAmount
                    ]);
                    if(!$invoice->save()) {
                        return $invoice->getErrors();
                    }
                }


//            invoice for driver
                $driver = $this->driver;
                $invoice = new Invoice([
                    'type' => Invoice::TYPE_DRIVER,
                    'user_id' => $driver->user->id,
                    'object_id' => $this->id,
                    'amount' => $this->countMoney('driver')
                ]);
                if(!$invoice->save()) {
                    return $invoice->getErrors();
                }

                $driver->balance += $invoice->amount;

                return $driver->save();

            } else {
                return $this->getErrors();
            }
        }

        return 'This trip is already finished.';
    }


}
