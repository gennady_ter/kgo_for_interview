<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "invoice".
 *
 * @property int $id
 * @property int $user_id
 * @property int $object_id
 * @property int $type
 * @property double $amount
 * @property string $created_at
 *
 * @property Trip $trip
 * @property User $user
 * @property float $driversBalance
 * @property int $familyId
 */
class Invoice extends \yii\db\ActiveRecord
{
    const TYPE_TRIP = 0;
    const TYPE_DRIVER = 1;
    const TYPE_PENALTY = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invoice';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'amount'], 'required'],
            [['user_id', 'object_id', 'type'], 'integer'],
            [['amount'], 'number'],
            [['created_at'], 'safe'],
            [['object_id'], 'exist', 'skipOnError' => true, 'targetClass' => Trip::className(), 'targetAttribute' => ['object_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'object_id' => 'Trip ID',
            'type' => 'Type',
            'amount' => 'Amount',
            'created_at' => 'Date created',
        ];
    }

    public function typeLabels () {
        return [
            self::TYPE_TRIP => 'Trip payment',
            self::TYPE_DRIVER => 'Driver Earning',
            self::TYPE_PENALTY => 'Penalty',
        ];
    }


    /**
    * @return mixed
    */
    public function getTypeName() {
        return $this->typeLabels()[$this->type];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrip()
    {
        return $this->hasOne(Trip::className(), ['id' => 'object_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getDriversBalance() {
        return $this->amount*0.8;
    }

    /**
     * @return int|null
     */
    public function getFamilyId() {
        if($this->user && $this->user->family) {
            return $this->user->family->id;
        } else {
            return null;
        }
    }
}
