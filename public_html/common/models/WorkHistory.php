<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "work_history".
 *
 * @property int $id
 * @property int $driver_id
 * @property string $date_begin
 * @property string $date_end
 * @property string $company
 * @property string $occupation
 *
 * @property Driver $driver
 */
class WorkHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'work_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['driver_id', 'date_begin', 'date_end', 'company', 'occupation'], 'required'],
            [['driver_id'], 'integer'],
            [['date_begin', 'date_end'], 'safe'],
            [['company', 'occupation'], 'string', 'max' => 255],
            [['driver_id'], 'exist', 'skipOnError' => true, 'targetClass' => Driver::className(), 'targetAttribute' => ['driver_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'driver_id' => 'Driver ID',
            'date_begin' => 'Date Begin',
            'date_end' => 'Date End',
            'company' => 'Company',
            'occupation' => 'Occupation',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDriver()
    {
        return $this->hasOne(Driver::className(), ['id' => 'driver_id']);
    }
}
