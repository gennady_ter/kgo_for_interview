<?php

namespace common\models;

use backend\components\MinutesHelper;
use Yii;

/**
 * This is the model class for table "nanny_service".
 *
 * @property int $id
 * @property int $trip_id
 * @property string $created_at
 * @property string $finished_at
 *
 * @property Trip $trip
 * @property float $minutes
 */
class NannyService extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nanny_service';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['trip_id'], 'required'],
            [['trip_id'], 'integer'],
            [['created_at', 'finished_at'], 'safe'],
            [['trip_id'], 'exist', 'skipOnError' => true, 'targetClass' => Trip::className(), 'targetAttribute' => ['trip_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'trip_id' => 'Trip ID',
            'created_at' => 'Active Since',
            'finished_at' => 'Finished At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrip()
    {
        return $this->hasOne(Trip::className(), ['id' => 'trip_id']);
    }


    public function getMinutes() {
        return MinutesHelper::count($this->created_at, $this->finished_at);
    }
}
