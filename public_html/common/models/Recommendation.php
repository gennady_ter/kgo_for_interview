<?php

namespace common\models;

use Yii;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "recommendation".
 *
 * @property int $id
 * @property int $driver_id
 * @property int $type
 * @property string $firstname
 * @property string $lastname
 * @property string $phone_number
 * @property string $email
 * @property string $information
 * @property string $additional
 *
 * @property Driver $driver
 */
class Recommendation extends \yii\db\ActiveRecord
{
    const TYPE_PERSONAL = 0;
    const TYPE_WORK = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'recommendation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['driver_id', 'type', 'firstname', 'lastname', 'phone_number', 'email', 'information'], 'required'],
            [['driver_id', 'type'], 'integer'],
            [['information', 'additional'], 'string'],
            [['firstname', 'lastname', 'phone_number', 'email'], 'string', 'max' => 255],
            [['driver_id'], 'exist', 'skipOnError' => true, 'targetClass' => Driver::className(), 'targetAttribute' => ['driver_id' => 'id']],
            ['type', 'in', 'range' => array_keys($this->types())]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $labels = [
            'id' => 'ID',
            'driver_id' => 'Driver ID',
            'type' => 'Type',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'phone_number' => 'Phone Number',
            'email' => 'Email',
            'information' => 'Relationship',
            'additional' => 'Additional',
        ];
        if($this->type == $this::TYPE_WORK) {
            $labels['information'] = 'Post';
            $labels['additional'] = 'Company name';
        }

        return $labels;
    }

    public function types() {
        return [
            self::TYPE_PERSONAL => 'Personal',
            self::TYPE_WORK => 'Work'
        ];
    }

    /**
     * @return string
     */
    public function getTypeName() {
        return $this->types()[$this->type];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDriver()
    {
        return $this->hasOne(Driver::className(), ['id' => 'driver_id']);
    }
}
