<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "family".
 *
 * @property int $id
 * @property integer $user_id
 * @property string $device
 *
 * @property User $user
 */
class UserDevice extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_device';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'device'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User',
            'device' => 'Device code'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @param $userId
     * @return array
     */
    public static function getDevicesByUserId($userId) {
        return self::find()->where(['user_id' => $userId])->select('device')->column();
    }

}
