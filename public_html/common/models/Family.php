<?php

namespace common\models;

use common\components\Routes;
use common\components\Trips;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "family".
 *
 * @property int $id
 * @property string $lastname
 * @property string $description
 * @property string $banking_data
 * @property string $created_at
 * @property string $update_at
 *
 * @property Child[] $children
 * @property FamilyMember[] $familyMembers
 * @property Trip[] $trips
 * @property float $moneySpent
 */
class Family extends \yii\db\ActiveRecord
{
    use Trips;
    use Routes;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'family';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lastname'], 'required'],
            [['description'], 'string'],
            [['created_at', 'update_at'], 'safe'],
            [['lastname', 'banking_data'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lastname' => 'Lastname',
            'description' => 'Description',
            'banking_data' => 'Banking Data',
            'created_at' => 'Active Since',
            'update_at' => 'Update At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChildren()
    {
        return $this->hasMany(Child::className(), ['family_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFamilyMembers()
    {
        return $this->hasMany(FamilyMember::className(), ['family_id' => 'id']);
    }

    /**
     * @return FamilyMember|mixed
     */
    public function getFirstFamilyMember() {
        return $this->familyMembers ? $this->familyMembers[0] : null;
    }

    /**
     * @return array
     */
    public function getRoutes() {
        $routes = $this->getRelatedRoutes('children', 'routes');
        return $routes;
    }

    /**
     * @return array|Trip[]
     */
    public function getTrips() {
        return $this->getRelatedTrips('children', 'trips');
    }

    /**
     * @param $status
     * @return array
     */
    public function getTripsByStatus($status) {
        $trips = [];
        foreach ($this->routes as $route) {
            $trips = array_merge($trips, $route->getTrips()->where(['status' => $status])->all());
        }
        return $trips;
    }

    /**
     * @return array
     */
    public function getTripsMonth()
    {
        return $this->getRelatedTrips('children', 'tripsMonth');
    }
    /**
     * @return array
     */
    public function getTripsWeek()
    {
        return $this->getRelatedTrips('children', 'tripsWeek');
    }

    /**
     * @return float|int
     */
    public function getMoneySpent() {
        // todo: change using invoice
        $money = 0;
        foreach ($this->trips as $trip) {
            $money += $trip->countMoney('client');
        }
        return $money;
    }
}
