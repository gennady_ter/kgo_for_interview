<?php

namespace common\models;

use backend\components\ApiConfigTrait;
use common\components\TripInterface;
use common\components\Trips;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "child".
 *
 * @property int $id
 * @property int $family_id
 * @property int $driver_id
 * @property string $name
 * @property int $age
 * @property string $photo
 * @property string $school_district
 * @property string $school_name
 * @property string $school_address
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Occupation[] $occupations
 * @property Driver $driver
 * @property Family $family
 * @property GroupChildren[] $groupChildrens
 * @property Group[] $groups
 * @property Route[] $routes
 * @property Trip[] $groupTrips
 * @property Trip[] $childTrips
 * @property Trip[] $allTrips
 * @property string $fullname
 */
class Child extends \yii\db\ActiveRecord
{
    use Trips;
    use ApiConfigTrait;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'child';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['family_id', 'name', 'age', 'school_name', 'school_address'], 'required'],
            [['family_id', 'driver_id', 'age'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'school_district', 'school_name', 'school_address'], 'string', 'max' => 255],
            [['driver_id'], 'exist', 'skipOnError' => true, 'targetClass' => Driver::className(), 'targetAttribute' => ['driver_id' => 'id']],
            [['family_id'], 'exist', 'skipOnError' => true, 'targetClass' => Family::className(), 'targetAttribute' => ['family_id' => 'id']],
            [['!photo'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'family_id' => 'Family ID',
            'driver_id' => 'Driver ID',
            'name' => 'Name',
            'age' => 'Age',
            'photo' => 'Photo',
            'school_district' => 'School District',
            'school_name' => 'School Name',
            'created_at' => 'Active Since',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($this->validate()) {
            /**
             * @var $file UploadedFile
             */

            $file = UploadedFile::getInstance($this, 'photo');
            if($file) {
                $file->saveAs(Yii::getAlias('@common/uploads/child/') . $file->baseName . '.' . $file->extension);
                $this->photo = '/common/uploads/child/' . $file->baseName . '.' . $file->extension;
            } elseif(!$file && is_null($this->photo)) {
                return false;
            }

            return parent::beforeSave($insert);
        } else {
            return false;
        }
    }

    public function afterFind()
    {
        parent::afterFind();
        if($this->photo) {
            $this->photo = 'http://'.Yii::$app->getRequest()->serverName.$this->photo;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if($this->photo) {
            $this->photo = 'http://'.Yii::$app->getRequest()->serverName.$this->photo;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDriver()
    {
        return $this->hasOne(Driver::className(), ['id' => 'driver_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFamily()
    {
        return $this->hasOne(Family::className(), ['id' => 'family_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroupChildrens()
    {
        return $this->hasMany(GroupChildren::className(), ['child_id' => 'id']);
    }

    public function getGroups() {
        return ArrayHelper::getColumn($this->groupChildrens, 'group');
    }

    public function getCountChildren() {
        return 1;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoutes() {
        return $this->hasMany(Route::className(), ['child_id' => 'id'])
            ->indexBy('id')->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOccupations() {
        return $this->hasMany(Occupation::className(), ['child_id' => 'id']);
    }


    /**
     * @return Trip[]
     */
    public function getTrips() {
        return array_merge(
            $this->getRelatedTrips('routes', 'trips'),
            $this->getRelatedTrips('groups', 'trips')
        );
    }

    /**
     * @return array
     */
    public function getTripsMonth()
    {
        return array_merge(
            $this->getRelatedTrips('routes', 'tripsMonth'),
            $this->getRelatedTrips('groups', 'tripsMonth')
        );
    }

    /**
     * @return array
     */
    public function getTripsWeek()
    {
        return array_merge(
            $this->getRelatedTrips('routes', 'tripsWeek'),
            $this->getRelatedTrips('groups', 'tripsWeek')
        );
    }

    /**
     * @return string
     */
    public function getFullname() {
        return $this->name.' '.$this->family->lastname;
    }


    public static function getList() {

        $results = [];
        foreach (self::find()->all() as $child) {
            /** @var $child self */
            $results[$child->id] = $child->fullname;
        }

        return $results;
    }


    /**
     * @param $new
     * @return bool
     */
    public function updateOccupations($new) {

        $occupations = $this->getOccupations()->select('id')->column();
        if(!empty($occupations)) {
            Occupation::deleteAll(['in', 'id', array_diff($occupations, array_keys($new))]);
        }

        foreach ($new as $id => $value) {

            if($value['name'] && $value['address']) {
                $occupation = Occupation::findOne($id);
                if(!$occupation) {
                    $occupation = new Occupation(['child_id' => $this->id]);
                }
                $occupation->name = $value['name'];
                $occupation->address = $value['address'];

                if(!$occupation->save()) {
                    return false;
                }
            }
        }
        return true;
    }


}
