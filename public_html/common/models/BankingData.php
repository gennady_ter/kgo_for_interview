<?php

namespace common\models;

use api\components\BankingHelper;
use tuyakhov\jsonapi\LinksInterface;
use tuyakhov\jsonapi\ResourceInterface;
use tuyakhov\jsonapi\ResourceTrait;
use Yii;
use yii\helpers\Inflector;
use yii\helpers\Url;
use yii\httpclient\Client;
use yii\web\Link;

/**
 * This is the model class for table "banking_data".
 *
 * @property int $id
 * @property string $user_id
 * @property string $cardnumber
 * @property string $expiration_date
 * @property int $cvv
 *
 * @property Driver[] $drivers
 * @property User $user
 */
class BankingData extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'banking_data';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cardnumber', 'expiration_date', 'cvv'], 'required'],
            [['cvv', 'user_id'], 'integer'],
            [['cardnumber', 'expiration_date'], 'string', 'max' => 255],
            ['cardnumber', 'validateCardnumber']
        ];
    }

    /**
     * @param $attribute
     * @param $params
     * @throws \yii\base\InvalidConfigException
     */
    public function validateCardnumber($attribute, $params) {
        if(!BankingHelper::send($this->$attribute)) {
            $this->addError($attribute, 'Credit card number is invalid');
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cardnumber' => 'Cardnumber',
            'expiration_date' => 'Expiration Date',
            'cvv' => 'Cvv',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDrivers()
    {
        return $this->hasMany(Driver::className(), ['banking_data_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
