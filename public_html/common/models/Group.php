<?php

namespace common\models;

use api\components\PushHelper;
use common\components\Trips;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "group".
 *
 * @property int $id
 * @property string $name
 * @property string $created_at
 * @property string $updated_at
 *
 * @property GroupChildren[] $groupChildren
 * @property Child[] $children
 * @property Route $route
 * @property Route[] $routes
 * @property int $countChildren
 */
class Group extends ActiveRecord
{
    use Trips;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'name',
            'created_at' => 'Active Since',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return array
     */
    public function getChildren()
    {
        $children = [];
        foreach ($this->groupChildren as $relation) {
            $children[] = $relation->child;
        }
        return $children;
    }

    /**
     * @return int|string
     */
    public function getCountChildren() {
        return $this->getGroupChildren()->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroupChildren() {
        return $this->hasMany(GroupChildren::className(), ['group_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getRoutes() {
        return $this->hasMany(Route::className(), ['group_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getRoute() {
        return $this->hasOne(Route::className(), ['group_id' => 'id'])
            ->where(['route.is_main' => true]);
    }

    /**
     * @return Trip[]
     */
    public function getTrips() {
        return $this->getRelatedTrips('routes', 'trips');
    }

    /**
     * @return Trip[]
     */
    public function getTripsMonth() {
        return $this->getRelatedTrips('routes', 'tripsMonth');
    }

    /**
     * @return Trip[]
     */
    public function getTripsWeek() {
        return $this->getRelatedTrips('routes', 'tripsWeek');
    }

    /**
     * @param $children
     * @return array|null|ActiveRecord
     */
    public static function findByChildren($children) {
        $group = Group::find()->leftJoin(GroupChildren::tableName().' gch', 'group.id = gch.group_id');

        $cond = ['and'];
        foreach ($children as $child) {
            $cond[] = ['in',
                'group_id',
                GroupChildren::find()->select('group_id')->where(['child_id' => $child])
            ];
        }
        return $group->where($cond)->having(['count(group.id)' => count($children)])->groupBy('group_id')->one();
    }

    /**
     * @return bool|Group
     * @throws \yii\base\Exception
     */
    public static function create() {
        $group = new Group();
        $group->name = self::generateUniqueRandomString();
        return $group->save() ? $group : false;
    }

    /**
     * @param $childId
     * @return bool
     */
    public function addChild($childId) {
        $groupChild = GroupChildren::findOne([
            'group_id' => $this->id,
            'child_id' => $childId
        ]);
        if(!$groupChild) {
            $groupChild = new GroupChildren([
                'group_id' => $this->id,
                'child_id' => $childId
            ]);
            if($groupChild->save() && $this->route){
                return $this->route->createChildRoute($childId);
            }
        }
        return false;
    }


    /**
     * @param int $length
     * @return string
     * @throws \yii\base\Exception
     */
    public static function generateUniqueRandomString($length = 6) {

        $randomString = Yii::$app->getSecurity()->generateRandomString($length);

        if(!self::findOne(['name' => $randomString])) {
            return $randomString;
        } else {
            return self::generateUniqueRandomString($length);
        }

    }

    /**
     * @param $name
     * @return null|static
     */
    public static function findByName($name) {
        return self::findOne(['name' => $name]);
    }

    /**
     * @return array
     */
    public function getAllPoints(){
        $points = [];
        foreach ($this->routes as $route) {
            if(!in_array($route->point_a, $points)) {
                $points[] = $route->point_a;
            }
            if(!in_array($route->point_b, $points)) {
                $points[] = $route->point_b;
            }
        }

        return $points;
    }


    /**
     * @return array|FamilyMember[]
     */
    public function getFamilyMembers() {
        $familyMembers = [];
        foreach ($this->routes as $route) {
            if(!array_key_exists($route->family_member_id, $familyMembers)) {
                $familyMembers[$route->family_member_id] = $route->familyMember;
            }
        }

        return $familyMembers;
    }

    /**
     * @return array|Family[]
     */
    public function getFamilies() {
        $families = [];
        foreach ($this->getFamilyMembers() as $familyMember) {
            $families[] = $familyMember->family;
        }

        return $families;
    }
}
