<?php

namespace common\models;

use backend\components\ApiConfigTrait;
use common\components\UserRelated;

/**
 * This is the model class for table "family_member".
 *
 * @property int $id
 * @property int $user_id
 * @property int $family_id
 * @property string $role
 * @property string $occupation
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 *
 * @property User $user
 * @property Family $family
 * @property Trip[] $trips
 * @property Route[] $routes
 */
class FamilyMember extends UserRelated
{
    use ApiConfigTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'family_member';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'family_id'], 'required'],
            [['user_id', 'family_id'], 'integer'],
            [['description'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['role', 'occupation'], 'string', 'max' => 255],
            [['family_id'], 'exist', 'skipOnError' => true, 'targetClass' => Family::className(), 'targetAttribute' => ['family_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'family_id' => 'Family ID',
            'role' => 'Role',
            'occupation' => 'Occupation',
            'description' => 'Description',
            'created_at' => 'Active Since',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFamily()
    {
        return $this->hasOne(Family::className(), ['id' => 'family_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrips()
    {
        return $this->hasMany(Trip::className(), ['family_member_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoutes()
    {
        return $this->hasMany(Route::className(), ['family_member_id' => 'id']);
    }


    /**
     * @param $statuses array
     * @return array
     */
    public function getTripsByStatuses($statuses) {
        return $this->getTrips()
            ->rightJoin('route', 'trip.route_id = route.id')
            ->andWhere('route.group_id is null')
            ->orWhere('route.group_id is not null and route.is_main = 1')
            ->andWhere(['in', 'trip.status', $statuses])
            ->all();
    }

}
