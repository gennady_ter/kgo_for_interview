<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "feedback".
 *
 * @property int $id
 * @property int $user_id
 * @property string $email
 * @property string $message
 * @property int $reply_mail_id
 *
 * @property User $user
 */
class Feedback extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'feedback';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'reply_mail_id'], 'integer'],
            [['email', 'message'], 'required'],
            [['message'], 'string'],
            [['email'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'email' => 'Email',
            'message' => 'Message',
            'reply_mail_id' => 'Reply Mail ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return array
     */
    public static function getList() {
        return self::find()->select('id')->indexBy('id')->column();
    }

    public function beforeSave($insert)
    {
        if($insert) {
            /** @var User $user */
            $user = Yii::$app->user->getIdentity();
            Yii::$app->mailer->compose()
                ->setTo(Yii::$app->params['managerEmail'])
                ->setFrom([$this->email => $user->firstname.' '.$user->lastname])
                ->setSubject('Feedback from '.Yii::$app->params['projectName'])
                ->setTextBody($this->message)
                ->send();
        }
        return parent::beforeSave($insert);
    }
}
