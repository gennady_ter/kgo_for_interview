<?php
namespace common\models;

use tuyakhov\jsonapi\Inflector;
use tuyakhov\jsonapi\LinksInterface;
use tuyakhov\jsonapi\ResourceInterface;
use tuyakhov\jsonapi\ResourceTrait;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Url;
use yii\web\IdentityInterface;
use yii\web\Link;
use yii\web\UploadedFile;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property int $type
 * @property string $email
 * @property string $photo
 * @property int $status
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property int $created_at
 * @property int $updated_at
 * @property string $address
 * @property string $lastname
 * @property string $firstname
 * @property int $rating
 * @property string $device
 *
 * @property Driver|null $driver
 * @property FamilyMember|null $familyMember
 * @property Family|null $family
 * @property Invoice[] $invoices
 * @property Notification[] $notifications
 * @property Review[] $reviews
 * @property BankingData[] $bankingData
 * @property UserDevice[] $userDevices
 */
class User extends ActiveRecord implements IdentityInterface
{

    const STATUS_DISABLED = 0;
    const STATUS_ACTIVE = 10;

    const TYPE_FAMILY_MEMBER = 0;
    const TYPE_DRIVER = 1;
    const TYPE_FULL = 2;

    protected $passwordValue;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'auth_key', 'password_hash', 'lastname', 'firstname'], 'required'],
            [['created_at', 'updated_at', 'photo'], 'safe'],
            [['type', 'status', 'created_at', 'updated_at', 'rating'], 'integer'],
            [['username', 'email', 'password_hash', 'password_reset_token', 'address', 'lastname', 'firstname'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['username', 'email'], 'unique'],
            [['password_reset_token'], 'unique'],
            //todo: сделать загрузку файлов в base64
//            [['!photo'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Phone number',
            'type' => 'Type',
            'email' => 'Email',
            'photo' => 'Photo',
            'status' => 'Status',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'created_at' => 'Active Since',
            'updated_at' => 'Updated At',
            'address' => 'Address',
            'lastname' => 'Lastname',
            'firstname' => 'Firstname',
            'rating' => 'Rating',
        ];
    }
    
    
    public function statuses() {
        return [
            self::STATUS_ACTIVE => 'Active',
            self::STATUS_DISABLED => 'Disabled',
        ];
    }

    public function statusName() {
        return $this->statuses()[$this->status];
    }


    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return User::findOne(['auth_key' => $token]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    public function resetPassword() {
        $this->auth_key = null;
        $this->password_hash = null;
        $this->password_reset_token = null;
        return $this->save();
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * @param int $length
     * @return string
     */
    public function generatePassword($length = 4){
        $number = '';
        for ($i = 0; $i < $length; $i++) {
            $number .= mt_rand(0, 9);
        }
        return $number;
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getDriver()
    {
        if($this->isDriver()) {
            return $this->hasOne(Driver::className(), ['user_id' => 'id']);
        } else {
            return null;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFamilyMember()
    {
        if($this->isFamilyMember()) {
            return $this->hasOne(FamilyMember::className(), ['user_id' => 'id']);
        } else {
            return null;
        }
    }

    /**
     * @return Family|null
     */
    public function getFamily()
    {
        if($this->familyMember) {
            return $this->familyMember->family;
        } else {
            return null;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoices()
    {
        return $this->hasMany(Invoice::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotifications()
    {
        return $this->hasMany(Notification::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviews()
    {
        return $this->hasMany(Review::className(), ['user_id' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBankingData()
    {
        return $this->hasMany(BankingData::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserDevices()
    {
        return $this->hasMany(UserDevice::className(), ['user_id' => 'id']);
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if(!$this->validate()) return false;
        if ($this->photo) {
            /**
             * @var $file UploadedFile
             */
            $file = UploadedFile::getInstance($this, 'photo');
            if($file) {
                $file->saveAs(Yii::getAlias('@common/uploads/user/') . $file->baseName . '.' . $file->extension);
                $this->photo = '/common/uploads/user/' . $file->baseName . '.' . $file->extension;
            } else {
                $this->photo = str_replace('http://'.Yii::$app->getRequest()->serverName, '', $this->photo);
            }
        }
        return parent::beforeSave($insert);
    }

    /**
     * @param null $password
     * @return bool
     */
    public function createNew($password = null)
    {
        $this->generateAuthKey();

        $this->passwordValue = $password ? $password : $this->generatePassword();
        $this->setPassword($this->passwordValue);

        if($this->isDriver()) {
            Yii::$app->mailer->compose()
                ->setTo(Yii::$app->params['managerEmail'])
                ->setFrom([$this->email => Yii::$app->params['projectName']])
                ->setSubject('New driver')
                ->setTextBody('Check new driver '.$this->firstname.' '.$this->lastname.' with number '.$this->username)
                ->send();
        }

        return $this->save(false);
    }

    /**
     * @return bool
     */
    public function isDriver() {
        return $this->type == $this::TYPE_DRIVER || $this->type == $this::TYPE_FULL;
    }

    /**
     * @return bool
     */
    public function isFamilyMember() {
        return $this->type == $this::TYPE_FAMILY_MEMBER || $this->type == $this::TYPE_FULL;
    }

    public static function getList() {
        return self::find()->select('username')->indexBy('id')->column();
    }

    public function uploadFromBase64() {
        if($this->photo) {
            if (preg_match('/^data:image\/(\w+);base64,/', $this->photo, $type)) {
                $data = substr($this->photo, strpos($this->photo, ',') + 1);
                $type = strtolower($type[1]); // jpg, png, gif

                if (!in_array($type, [ 'jpg', 'jpeg', 'gif', 'png' ])) {
                    throw new \Exception('invalid image type');
                }

                $data = base64_decode($data);

                if ($data === false) {
                    throw new \Exception('base64_decode failed');
                }
            } else {
                throw new \Exception('did not match data URI with image data');
            }

            file_put_contents(Yii::getAlias('@common/uploads/user/').Yii::$app->security->generateRandomString(8).".{$type}", $data);

            $this->photo = '/common/uploads/user/'.Yii::$app->security->generateRandomString(8).".{$type}";
        }
    }

    public function afterFind()
    {
        parent::afterFind();
        if($this->photo) {
            $this->photo = 'http://'.Yii::$app->getRequest()->serverName.$this->photo;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if($this->photo) {
            $this->photo = 'http://'.Yii::$app->getRequest()->serverName.$this->photo;
        }
    }
}
