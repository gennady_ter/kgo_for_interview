<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "inverview".
 *
 * @property int $id
 * @property int $family_member_id
 * @property int $driver_id
 * @property int $route_id
 * @property string $date_from
 * @property string $date_to
 * @property string $time_from
 * @property string $time_to
 * @property int $type
 * @property int $status
 * @property-read FamilyMember $familyMember
 * @property-read Driver $driver
 * @property-read Route $route
 */
class Interview extends \yii\db\ActiveRecord
{

    const TYPE_ONLINE = 0;
    const TYPE_FACE_TO_FACE = 1;

    const STATUS_AWAITING = 0;
    const STATUS_APPROVED = 1;
    const STATUS_DECLINED = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'inverview';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['family_member_id', 'driver_id', 'route_id', 'date_from', 'date_to', 'time_from', 'time_to'], 'required'],
            [['family_member_id', 'driver_id', 'route_id', 'type', 'status'], 'integer'],
            [['date_from', 'date_to', 'time_from', 'time_to'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'family_member_id' => 'Family Member ID',
            'driver_id' => 'Driver ID',
            'route_id' => 'Route ID',
            'date_from' => 'Date From',
            'date_to' => 'Date To',
            'time_from' => 'Time From',
            'time_to' => 'Time To',
            'type' => 'Type',
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFamilyMember() {
        return $this->hasOne(FamilyMember::className(), ['id' => 'family_member_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDriver() {
        return $this->hasOne(Driver::className(), ['id' => 'driver_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoute() {
        return $this->hasOne(Route::className(), ['id' => 'route_id']);
    }

    /**
     * @return bool
     */
    public function approve() {
        $this->status = $this::STATUS_APPROVED;
        return $this->save();
    }

    /**
     * @return bool
     */
    public function decline() {
        $this->status = $this::STATUS_DECLINED;
        return $this->save();
    }


    public function beforeSave($insert)
    {
        if($this->isNewRecord) {
            // todo: check email
//            Yii::$app->mailer->compose()
//                ->setTo($this->driver->user->email)
//                ->setFrom([$this->familyMember->user->email => $this->familyMember->user->firstname.' '.$this->familyMember->user->lastname])
//                ->setSubject('Request for participation in the trip')
//                ->setTextBody('User is inviting you to participate the trip')
//                ->send();
        }
        return parent::beforeSave($insert);
    }

}
