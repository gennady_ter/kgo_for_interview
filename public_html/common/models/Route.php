<?php

namespace common\models;

use backend\components\ApiConfigTrait;
use backend\components\GoogleMapsHelper;
use api\components\PushHelper;
use common\components\Trips;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "route".
 *
 * @property int $id
 * @property string $point_a
 * @property string $point_b
 * @property int $is_roundtrip
 * @property int $group_id
 * @property int $child_id
 * @property int $driver_id
 * @property int $family_member_id
 * @property double $nanny_hours
 * @property double $wait_hours
 * @property string $date_start
 * @property string $date_end
 * @property string $days_of_week
 * @property string $time
 * @property string $created_at
 * @property string $updated_at
 * @property string $status
 * @property string $money
 * @property string $is_main
 *
 * @property string $fullname
 * @property Group|Child $object
 * @property Child $child
 * @property Group $group
 * @property Driver $driver
 * @property FamilyMember $familyMember
 * @property Interview[] $interviews
 */
class Route extends ActiveRecord
{
    use Trips;
    use ApiConfigTrait;

    const STATUS_AWAITING = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_ARCHIVE = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'route';
    }

    public static function relatedAttribute()
    {
        return 'route_id';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['point_a', 'point_b', 'child_id', 'date_start', 'date_end', 'days_of_week', 'time'], 'required'],
            [['point_a', 'point_b'], 'string'],
            [['child_id', 'nanny_hours', 'wait_hours', 'driver_id', 'family_member_id', 'status'], 'integer'],
            [['date_start', 'date_end', 'time', 'created_at', 'updated_at', 'money', 'is_main'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'point_a' => 'Pick Up Location',
            'point_b' => 'Destination',
            'is_roundtrip' => 'Is Roundtrip',
            'group_id' => 'Group ID',
            'child_id' => 'Object ID',
            'driver_id' => 'Driver ID',
            'family_member_id' => 'Family member ID',
            'nanny_hours' => 'Nanny Hours',
            'wait_hours' => 'Wait Hours',
            'date_start' => 'Date Start',
            'date_end' => 'End Time',
            'days_of_week' => 'Days Of Week',
            'time' => 'Pick Up Time',
            'created_at' => 'Active Since',
            'updated_at' => 'Updated At',
            'status' => 'Status',
            'money' => 'Money',
            'is_main' => 'Is main',
        ];
    }

    public function dayLabels($day = null) {
        $days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
        if($day) {
            return $days[$day];
        } else {
            return ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
        }
    }

    public function statusLabels() {
        return [
            'Planned',
            'Completed',
            'In progress',
        '   Cancelled'
        ];
    }

    /**
     * @return mixed
     */
    public function getStatusName() {
        return $this->statusLabels()[$this->status];
    }

    /**
     * @return ActiveQuery
     */
    public function getDriver() {
        return $this->hasOne(Driver::className(), ['id' => 'driver_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getFamilyMember() {
        return $this->hasOne(FamilyMember::className(), ['id' => 'family_member_id']);
    }

    /**
     * @return Family
     */
    public function getFamily() {
        return $this->familyMember->family;
    }

    /**
     * @return ActiveQuery
     */
    public function getInterviews() {
        return $this->hasMany(Interview::className(), ['route_id' => 'id']);
    }

    /**
     * @return Interview|null
     */
    public function getCurrentInterview()
    {
        return Interview::findOne([
            'route_id' => $this->id,
            'status' => Interview::STATUS_AWAITING
        ]);
    }


    public function createChildRoute($childId) {
        /** @var User $familyMember */
        $familyMember = Yii::$app->user->getIdentity();
        $familyMember = $familyMember->familyMember;

        $route = new Route([
            'point_a' => $this->point_a,
            'point_b' => $this->point_b,
            'is_roundtrip' => $this->is_roundtrip,
            'group_id' => $this->group_id,
            'child_id' => $childId,
            'driver_id' => $this->driver_id,
            'nanny_hours' => $this->nanny_hours,
            'wait_hours' => $this->wait_hours,
            'date_start' => $this->date_start,
            'date_end' => $this->date_end,
            'days_of_week' => $this->days_of_week,
            'time' => $this->time,
            'status' => $this->status,
            'is_main' => false,
        ]);
        if($familyMember) {
            $route->family_member_id = $familyMember->id;
        } else {
            $route->family_member_id = $this->family_member_id;
        }

        return $route->save() ? $route : false;

    }

    /**
     * @param $interviewData
     * @return Interview
     */
    public function chooseDriver($interviewData) {
        $interview = new Interview($interviewData);
        $interview->save();

        $this->driver_id = $interview->driver_id;
        $this->countMoney();
        $this->save();

        $trip = $this->getNoDriverTrip();
        $trip->driver_id = $interview->driver_id;
        $trip->save();

        return $interview;
    }


    public function declineDriver() {
        $interview = $this->getCurrentInterview();
        if(!$interview) {
            return false;
        }

        PushHelper::send($interview->driver, 'Sorry, you have been declined.');

        return $interview->decline();
    }

    public function approveDriver($driverId = null) {
        $interview = $this->getCurrentInterview();
        if($interview) {
            $interview->approve();
        }

        $trip = $this->getNoDriverTrip();
        if(!$trip) {
            return false;
        }

        if(!is_null($driverId)) {
            $trip->driver_id = $this->driver_id = $driverId;
            if($this->group_id) {
                foreach ($this->group->routes as $route) {
                    $route->setDriver($driverId);
                }
            }
        }

        PushHelper::send($trip->driver, 'Congratulations! You have been approved.');

        $trip->status = $trip::STATUS_AWAITING_ALL;
        $this->countMoney();

        return $this->save() && $trip->save();
    }


    public function setDriver($driverId) {
        $trip = $this->getNoDriverTrip();
        if(!$trip) {
            return false;
        }
        $this->driver_id = $trip->driver_id = $driverId;

        return $this->save() && $trip->save();
    }

    /**
     * @return ActiveQuery | bool
     */
    public function getGroup() {
        return $this->hasOne(Group::className(), ['id' => 'group_id']);
    }

    /**
     * @return ActiveQuery | bool
     */
    public function getChild() {
        return $this->hasOne(Child::className(), ['id' => 'child_id']);
    }

    /**
     * @return int|string
     */
    public function getTripsTotal() {
        return $this->getTrips()->count();
    }

    /**
     * @return int|string
     */
    public function getTripsMonthTotal() {
        return $this->getTripsMonth()->count();
    }
    /**
     * @return int|string
     */
    public function getTripsWeekTotal() {
        return $this->getTripsWeek()->count();
    }


    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert) {
        if(is_array($this->days_of_week)) {
            $this->days_of_week = serialize($this->days_of_week);
        }
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        if($insert) {
            $trip = new Trip([
                'route_id' => $this->id,
                'family_member_id' => $this->family_member_id,
            ]);
            if(!$this->driver_id) {
                $trip->status = $trip::STATUS_NO_DRIVER;
            } else {
                $trip->driver_id = $this->driver_id;
            }
            $trip->save();
        }
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @return string
     */
    public function getFullname() {
        return $this->point_a.' - '.$this->point_b.' ('.$this->time.')';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObject() {
        if($this->group_id) {
            return $this->getGroup();
        } else {
            return $this->getChild();
        }
    }

    /**
     * @return array
     */
    public function getCommonValues() {
        return $this->getAttributes(['id', 'group_id', 'wait_hours', 'date_start', 'date_end', 'days_of_week']);
    }

    /**
     * @return array
     */
    public function getUniqueValues() {
        return $this->getAttributes(['id', 'point_a', 'point_b', 'is_roundtrip', 'time', 'money']);
    }

    /**
     * @return array|Trip|null|ActiveRecord
     */
    public function getNoDriverTrip() {
        return $this->getTrips()->where(['status' => Trip::STATUS_NO_DRIVER])->one();
    }


    public function countMoney($for = 'client') {
        $segmentData = GoogleMapsHelper::getSegmentData($this->point_a, $this->point_b);
        if($segmentData) {
            $minutes = $segmentData->duration->value;
            $distance = $segmentData->distance->value/1609.34;

            $settings = Yii::$app->params['settings'];

            $money = ($settings['cost_tripindividual'] * $minutes)
                + ($distance/$this->driver->car->efficiency) * $settings['gas_price'];

            if($this->wait_hours) {
                $money += $settings['cost_wait'] * $this->wait_hours;
            }

            if($this->nanny_hours) {
                $money += $this->nanny_hours * $settings['cost_nannyservice'];
            }

            if($for == 'driver') {
                $money *= 0.8;
            } elseif($for == 'service') {
                $money *= 0.2;
            } elseif($for == 'client' & $this->group_id) {
                // todo: add for brothers and sisters
                $money /= $this->object->countChildren;
            }
            $this->money = number_format($money, 2, '.', '');
        }

    }
}
