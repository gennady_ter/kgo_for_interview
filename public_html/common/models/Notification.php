<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "notification".
 *
 * @property int $id
 * @property int $user_id
 * @property string $message
 * @property int $status
 * @property int $code
 * @property string $notes
 * @property string $created_at
 *
 * @property User $user
 */
class Notification extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notification';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'code', 'message'], 'required'],
            [['user_id'], 'integer'],
            [['message'], 'string'],
            [['created_at', 'code', 'status'], 'safe'],
            [['notes'], 'string', 'max' => 255],
            ['code', 'in', 'range' => array_keys($this->codeLabels())],
            ['status', 'in', 'range' => array_keys($this->statusLabels())],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'message' => 'Additional info',
            'code' => 'Reason',
            'status' => 'Status',
            'notes' => 'Notes',
            'created_at' => 'Date and Time',
        ];
    }

    public function codeLabels() {
        return [
            'Car arrived',
            'Trip is cancelled',
            'Trip is in progress',
        ];
    }

    /**
    * @return mixed
    */
    public function getCodeName() {
        return $this->codeLabels()[$this->code];
    }

    public function statusLabels() {
        return [
            'Not read',
            'Resolved',
            'In progress',
        ];
    }

    /**
    * @return mixed
    */
    public function getStatusName() {
        return $this->statusLabels()[$this->status];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
