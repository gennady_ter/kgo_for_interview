<?php

namespace backend\tests\functional;

use backend\tests\FunctionalTester;
use common\fixtures\UserFixture;

/**
 * Class LoginCest
 */
class DriverCest
{
    /**
     * Load fixtures before db transaction begin
     * Called in _before()
     * @see \Codeception\Module\Yii2::_before()
     * @see \Codeception\Module\Yii2::loadFixtures()
     * @return array
     */
    public function _fixtures()
    {
        return [
            'user' => [
                'class' => UserFixture::className(),
                'dataFile' => codecept_data_dir() . 'login_data.php'
            ]
        ];
    }
    
    /**
     * @param FunctionalTester $I
     */
    public function create(FunctionalTester $I)
    {
        $I->wantTo('ensure that user authorizations works');
        $I->sendPOST('/user/login', ['username' => 'perkin', 'password' => 'tbgapi2018']);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->token = $I->getResponse();
        $I->setAuthToken();
    }
}
