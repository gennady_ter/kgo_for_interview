<?php
namespace backend\components;

use yii\base\Component;

class MinutesHelper extends Component {

    public static function count($start, $finish) {
        $start = strtotime($start);
        $finish = strtotime($finish);
        return ceil(abs($finish - $start) / 60.2);
    }

}