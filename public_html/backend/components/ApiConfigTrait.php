<?php
namespace backend\components;

use backend\models\ApiConfig;

trait ApiConfigTrait {

    public function getApiAttributes() {
        $attributes = \Yii::$app->cache->get(\Yii::$app->controller->id.'/'.\Yii::$app->controller->action->id);
        if(!$attributes) {
            $attributes = ApiConfig::getAttributesList(\Yii::$app->controller->id, \Yii::$app->controller->action->id);
            \Yii::$app->cache->set(\Yii::$app->controller->id.'/'.\Yii::$app->controller->action->id, $attributes);
        }

        $values = [];

        if($attributes) {
            foreach ($attributes as $i => $attribute) {
                if(strpos($attribute, '.')) {
                    unset($attributes[$i]);
                    $parts = explode('.', $attribute);
                    $model = $parts[0];
                    $modelAttribute = $parts[1];
                    if(!isset($values[$model])) {
                        $values[$model] = [];
                    }
                    if($this->$model) {
                        $values[$model][$modelAttribute] = $this->$model->$modelAttribute;
                        if(isset($parts[2]) && $this->$model->$modelAttribute) {
                            $modelSubAttribute = $parts[2];
                            $values[$model][$modelAttribute][$modelSubAttribute] = $this->$model->$modelAttribute->$modelSubAttribute;
                        }
                    } else {
                        $values[$model] = null;
                    }
                } else {
                    $values[$attribute] = $this->$attribute;
                }
            }
        } else {
            $values = $this->getAttributes();
        }

        return $values;
    }

}