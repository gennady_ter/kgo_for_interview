<?php
namespace backend\components;

use yii\base\Component;
use yii\helpers\Html;

class PhotoHelper extends Component {


    public static function render($model, $attribute) {

        if($model->$attribute) {
            echo Html::beginTag('a', ['href' => $model->$attribute, 'target' => '_blank']);
                echo Html::img($model->$attribute, ['class' => 'img-thumbnail']);
            echo Html::endTag('a');
        } else {
            echo Html::tag('p', 'No user\'s photo!', ['class' => 'alert alert-danger']);
        }
    }

}