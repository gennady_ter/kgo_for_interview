<?php
namespace backend\components;

use yii\base\Component;

class GoogleMapsHelper extends Component {

    const KEY = 'AIzaSyD5cgsIdP9soN7xzVb8u7U0KlETjSJbsiw';

    /**
     * @param $address
     * @return bool
     */
    public static function geocode($address) {
        $ch = curl_init();

        $url = http_build_query([
            'address' => $address,
            'key' => self::KEY
        ]);
        // set url
        curl_setopt(
            $ch,
            CURLOPT_URL,
            "https://maps.google.com/maps/api/geocode/json?".$url
        );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = json_decode(curl_exec($ch));
        curl_close($ch);

        if($result->status == 'OK') {
            return implode(',', (array)$result->results[0]->geometry->location);
        } else {
            return false;
        }
    }

    /**
     * @param $origin
     * @param $destination
     * @return bool | \stdClass
     */
    public static function getSegmentData($origin, $destination) {
        $origin = self::geocode($origin);
        $destination = self::geocode($destination);
        if($origin && $destination) {
            $ch = curl_init();
            curl_setopt(
                $ch,
                CURLOPT_URL,
                "https://maps.googleapis.com/maps/api/directions/json?origin=".$origin.'&destination='.$destination
            );
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $result = json_decode(curl_exec($ch));
            curl_close($ch);

            if($result->status == 'OK') {
                return $result->routes[0]->legs[0];
            }
        }

        return false;

    }

}