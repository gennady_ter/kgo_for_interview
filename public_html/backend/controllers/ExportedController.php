<?php
namespace backend\controllers;

use backend\models\DriverSearch;
use common\models\Driver;
use moonland\phpexcel\Excel;
use yii\web\Controller;

class ExportedController extends Controller {

    public function actionDrivers($period = null) {

        $drivers = DriverSearch::getActiveByTrips($period)->getModels();

        $d = Excel::export([
            'models' => $drivers,
            'savePath' => \Yii::getAlias('@app/web/export/'),
            'columns' => [
                'id',
//                'firstname',
//                'lastname',
//                'created_at',
//                'tripsTotal',
//                'tripsMonth',
//                'tripsWeek',
//                [
//                    'attribute' => 'user.rating',
//                    'value' => function($model){
//                        /** @var $model \common\models\Driver */
//                        return $model->rating.'('.$model->getReviews()->count().')';
//                    }
//                ],
                'expiration_date',
//                [
//                    'label' => 'Last activity',
//                    'value' => function($model) {
//                        /** @var $model \common\models\Driver */
//                        return $model->getLastActivity();
//                    }
//                ],
            ],
            'headers' => [
                'id' => 'ID',
                'expiration_date' => 'expiration_date',
            ],
        ]);


        $data = Excel::import(\Yii::getAlias('@app/web/export/test.xlsx'), [
            'setFirstRecordAsKeys' => true,
            'setIndexSheetByName' => true,
        ]);

        return $this->render('drivers', [
            'drivers' => $drivers
        ]);

    }

}