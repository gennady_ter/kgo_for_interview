<?php

namespace backend\controllers;

use Yii;
use common\models\Invoice;
use backend\models\InvoiceSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * InvoiceController implements the CRUD actions for Invoice model.
 */
class InvoiceController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @param int $type
     * @return string
     */
    public function actionIndex($type = Invoice::TYPE_TRIP)
    {
        $searchModel = new InvoiceSearch(['type' => $type]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $view = 'trip';

        if($type == Invoice::TYPE_DRIVER) {
            $view = 'driver';
        } elseif($type == Invoice::TYPE_PENALTY) {
            $view = 'penalty';
        }


        $total = Invoice::find()->where(['type' => $type])->count();

        return $this->render('index/'.$view, [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'statistics' => $total ? $dataProvider->getCount()/$total*100 : 0
        ]);
    }

    /**
     * Displays a single Invoice model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        if($model->type == Invoice::TYPE_TRIP) {
            $type = 'trip';
        } elseif($model->type == Invoice::TYPE_DRIVER) {
            $type = 'driver';
        } elseif($model->type == Invoice::TYPE_PENALTY) {
            $type = 'penalty';
        }
        return $this->render('view/'.$type, [
            'invoice' => $model,
        ]);
    }

    /**
     * Creates a new Invoice model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($type, $objectId)
    {
        $invoice = new Invoice(['type' => $type, 'object_id' => $objectId]);

        if ($invoice->load(Yii::$app->request->post()) && $invoice->save()) {
            return $this->redirect(Yii::$app->request->referrer);
        }

        return $this->render('create', [
            'invoice' => $invoice,
        ]);
    }

    /**
     * Updates an existing Invoice model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $invoice = $this->findModel($id);

        if ($invoice->load(Yii::$app->request->post()) && $invoice->save()) {
            return $this->redirect(['view', 'id' => $invoice->id]);
        }

        return $this->render('update', [
            'invoice' => $invoice,
        ]);
    }

    /**
     * Deletes an existing Invoice model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return null|Invoice
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = Invoice::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
