<?php
namespace backend\controllers;

use backend\models\FamilyMemberSearch;
use common\models\FamilyMember;
use common\models\User;
use yii\filters\VerbFilter;
use yii\web\Controller;
use Yii;
use yii\web\NotFoundHttpException;

class MemberController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Driver models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FamilyMemberSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * @param $familyId
     * @return string|\yii\web\Response
     */
    public function actionCreate($familyId) {
        $member = new FamilyMember(['family_id' => $familyId]);
        $user = new User(['type' => User::TYPE_DRIVER]);

        if($user->load(Yii::$app->request->post()) && $user->createNew()) {
            $member->user_id = $user->id;
            if ($member->load(Yii::$app->request->post()) && $member->save()) {
                return $this->redirect(['view', 'id' => $member->id]);
            }

        }

        return $this->render('create', [
            'member' => $member,
            'user' => $user,
        ]);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id) {
        $member = $this->findModel($id);
        $user = $member->user;

        if($user->load(Yii::$app->request->post()) && $user->createNew()
        && $member->load(Yii::$app->request->post()) && $member->save()) {
            return $this->redirect(['view', 'id' => $member->id]);
        }

        return $this->render('update', [
            'member' => $member,
            'user' => $user,
        ]);
    }

    /**
     * Displays a single Driver model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'member' => $this->findModel($id),
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Driver model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FamilyMember the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FamilyMember::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}