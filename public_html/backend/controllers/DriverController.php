<?php

namespace backend\controllers;

use common\models\BankingData;
use common\models\Car;
use common\models\JobHistory;
use common\models\Recommendation;
use common\models\Review;
use common\models\User;
use Yii;
use common\models\Driver;
use backend\models\DriverSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DriverController implements the CRUD actions for Driver model.
 */
class DriverController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Driver models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DriverSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $total = Driver::find()->count();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'statistics' => $total ? $dataProvider->getCount()/$total*100 : 0
        ]);
    }

    /**
     * Displays a single Driver model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $driver = $this->findModel($id);
        return $this->render('view', [
            'driver' => $driver,
            'user' => $driver->user,
            'car' => $driver->car,
        ]);
    }

    /**
     * Creates a new Driver model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $driver = new Driver();
        $user = new User(['type' => User::TYPE_DRIVER]);
        $car = new Car();
        $bankingData = new BankingData();

        if($user->load(Yii::$app->request->post()) && $user->createNew()) {
            $driver->user_id = $user->id;
            $bankingData->user_id = $user->id;
            if ($driver->load(Yii::$app->request->post()) && $driver->save()
                && $bankingData->load(Yii::$app->request->post()) && $bankingData->save()
            ) {
                $car->driver_id = $driver->id;
                if ($car->load(Yii::$app->request->post()) && $car->save()) {
                    return $this->redirect(['view', 'id' => $driver->id]);
                }
            }
        }

        return $this->render('create', [
            'driver' => $driver,
            'user' => $user,
            'car' => $car,
            'bankingData' => $bankingData
        ]);
    }

    /**
     * Updates an existing Driver model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $driver = $this->findModel($id);
        $user = $driver->user;
        $car = $driver->car;

        if($user->load(Yii::$app->request->post()) && $user->createNew()
            && $driver->load(Yii::$app->request->post()) && $driver->save()
            && $car->load(Yii::$app->request->post()) && $car->save()
        ) {
            return $this->redirect(['view', 'id' => $driver->id]);
        }

        return $this->render('update', [
            'driver' => $driver,
            'user' => $user,
            'car' => $car
            ]   );
    }

    /**
     * Deletes an existing Driver model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Driver model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Driver the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Driver::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionCreateJobHistory($id) {
        $driver = $this->findModel($id);
        $jobHistory = new JobHistory(['driver_id' => $driver->id]);
        if ($jobHistory->load(Yii::$app->request->post()) && $jobHistory->save()) {
            return $this->redirect(['view', 'id' => $driver->id]);
        }

        return $this->render('_forms/job-history', [
            'jobHistory' => $jobHistory,
        ]);
    }


    /**
     * @param $id
     * @param int $type
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionCreateRecommendation($id, $type = 0) {
        $driver = $this->findModel($id);
        $recommendation = new Recommendation([
            'type' => $type,
            'driver_id' => $driver->id
        ]);
        if($recommendation->validate('type')) {
            if ($recommendation->load(Yii::$app->request->post()) && $recommendation->save()) {
                return $this->redirect(['view', 'id' => $driver->id]);
            }

            return $this->render('_forms/recommendation', [
                'recommendation' => $recommendation,
            ]);
        } else {
            throw new NotFoundHttpException($recommendation->getFirstError('type'));
        }
    }
}
