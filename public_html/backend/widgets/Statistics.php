<?php
namespace backend\widgets;

use yii\base\Widget;
use yii\db\ActiveRecord;

class Statistics extends Widget {

    /** @var $model ActiveRecord */
    public $model;
    /** @var $condition array */
    public $condition;

    public function init() {

        $model = $this->model;
        $total = $model::find()->count();

        $current = $model::find()->where($this->condition)->count();

        return $current/$total;

    }

}