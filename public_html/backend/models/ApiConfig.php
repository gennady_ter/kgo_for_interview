<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "api_config".
 *
 * @property int $id
 * @property string $controller
 * @property string $action
 * @property string $attributes
 */
class ApiConfig extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'api_config';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['controller', 'action', 'attributes'], 'required'],
            [['attributes'], 'string'],
            [['controller', 'action'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'controller' => 'Controller',
            'action' => 'Action',
            'attributes' => 'Attributes',
        ];
    }

    /**
     * @param $controller
     * @param $action
     * @return null|static
     */
    public static function getAttributesList($controller, $action) {
        $config = self::findOne(['controller' => $controller, 'action' => $action]);
        $attributes = [];
        if($config) {
            $attributes = explode(',', $config->attributes);
        }
        return $attributes;
    }

}
