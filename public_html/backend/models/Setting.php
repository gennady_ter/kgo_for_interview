<?php

namespace backend\models;

use Yii;
use yii\base\BootstrapInterface;

/**
 * This is the model class for table "setting".
 *
 * @property int $id
 * @property string $param
 * @property string $value
 */
class Setting extends \yii\db\ActiveRecord implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'setting';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['param', 'value'], 'required'],
            [['param', 'value'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'param' => 'Param',
            'value' => 'Value',
        ];
    }


    public function bootstrap($app) {
        Yii::$app->params['settings'] = self::find()->select('value')->indexBy('param')->column();
    }
}
