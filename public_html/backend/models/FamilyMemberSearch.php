<?php

namespace backend\models;

use common\models\FamilyMember;
use common\models\User;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * FamilySearch represents the model behind the search form of `common\models\Family`.
 */
class FamilyMemberSearch extends FamilyMember
{
    public function attributes()
    {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), ['user.firstname', 'user.lastname', 'user.rating']);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['role', 'occupation', 'user.firstname', 'user.lastname', 'user.rating'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FamilyMember::find()->joinWith('user');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'created_at' => $this->created_at,
            'user.rating' => $this->getAttribute('user.rating'),
        ]);

        $query->andFilterWhere(['like', 'role', $this->role])
            ->andFilterWhere(['like', 'user.firstname', $this->getAttribute('user.firstname')])
            ->andFilterWhere(['like', 'user.lastname', $this->getAttribute('user.lastname')]);

        return $dataProvider;
    }
}
