<?php

namespace backend\models;

use common\models\Trip;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Driver;

/**
 * DriverSearch represents the model behind the search form of `common\models\Driver`.
 */
class DriverSearch extends Driver
{

    protected $tripTotalRanges = [
        'tripsTotal' => 0,
        'tripsWeekTotal' => 7,
        'tripsMonthTotal' => 30
    ];

    public function attributes()
    {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), [
            'firstname', 'lastname', 'rating', 'lastActivity'
        ], array_keys($this->tripTotalRanges));
    }
    public function attributeLabels()
    {
        // add related fields to searchable attributes
        return array_merge(parent::attributeLabels(), [
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'rating' => 'Rating',
            'tripsTotal' => 'Total trips',
            'tripsMonthTotal' => 'Trips for a month',
            'tripsWeekTotal' => 'Trips for a week',
            'lastActivity' => 'Last activity',
        ]);
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'sex', 'family_status'], 'integer'],
            [['description', 'birthdate', 'ethnicity', 'education', 'current_job', 'experience', 'resume', 'is_experienced_mom', 'drivers_license', 'expiration_date', 'available_from', 'available_to', 'created_at', 'updated_at',
                'firstname', 'lastname', 'rating',
                'tripsTotal', 'tripsMonthTotal', 'tripsWeekTotal', 'lastActivity'], 'safe'],
        ];
    }


    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Driver::find()->joinWith(['user']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $dataProvider->setSort([
            'attributes' => [
                'id',
                'firstname',
                'lastname',
                'created_at',
                'rating',
                'expiration_date',
                'tripsTotal' => [
                    'asc' => ['tripsTotal.count' => SORT_ASC],
                    'desc' => ['tripsTotal.count' => SORT_DESC]
                ],
                'tripsMonthTotal' => [
                    'asc' => ['tripsMonthTotal.count' => SORT_ASC],
                    'desc' => ['tripsMonthTotal.count' => SORT_DESC]
                ],
                'tripsWeekTotal' => [
                    'asc' => ['tripsWeekTotal.count' => SORT_ASC],
                    'desc' => ['tripsWeekTotal.count' => SORT_DESC]
                ],
                'lastActivity' => [
                    'asc' => ['trip.finished_at' => SORT_ASC],
                    'desc' => ['trip.finished_at' => SORT_DESC],
                ]
            ]
        ]);

//        set calculable sorting
        foreach ($this->tripTotalRanges as $range => $days) {
            $dataProvider->getSort()->attributes[$range] = [
                'asc' => [$range.'.count' => SORT_ASC],
                'desc' => [$range.'.count' => SORT_DESC]
            ];
            if($this->checkSort($range) || $this->$range) {
                $query->innerJoin($this->tripsQuery($range, $days), 'driver.id = '.$range.'.driver_id');
            }
        }
        if($this->checkSort('lastActivity')) {
            $query->innerJoin('trip', 'driver.id = trip.driver_id');
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
//             $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'driver.id' => $this->id,
            'user_id' => $this->user_id,
            'birthdate' => $this->birthdate,
            'sex' => $this->sex,
            'family_status' => $this->family_status,
            'expiration_date' => $this->expiration_date,
            'driver.created_at' => $this->created_at,
            'driver.updated_at' => $this->updated_at,
            'rating' => $this->rating,
        ]);


        $query->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'ethnicity', $this->ethnicity])
            ->andFilterWhere(['like', 'education', $this->education])
            ->andFilterWhere(['like', 'current_job', $this->current_job])
            ->andFilterWhere(['like', 'experience', $this->experience])
            ->andFilterWhere(['like', 'resume', $this->resume])
            ->andFilterWhere(['like', 'is_experienced_mom', $this->is_experienced_mom])
            ->andFilterWhere(['like', 'drivers_license', $this->drivers_license])
            ->andFilterWhere(['like', 'available_from', $this->available_from])
            ->andFilterWhere(['like', 'available_to', $this->available_to])
            ->andFilterWhere(['like', 'firstname', $this->firstname])
            ->andFilterWhere(['like', 'lastname', $this->lastname]);

        return $dataProvider;
    }


    protected function tripsQuery($name, $days = 0) {

        $query = '(SELECT driver_id, COUNT(*) AS count FROM trip';
        if($days) {
            $query .= ' WHERE finished_at > \''.date('Y-m-d H:i:s', strtotime('-'.$days.' days')).'\'';
        }
        $query .= ' GROUP BY driver_id ';
        if($this->$name != '') {
            $query .= 'HAVING count(*) ';
            $query .= $this->$name == 'active' ? '> 0' : '= '.$this->$name;
        }
        $query .= ') '.$name;
        return $query;
    }

    /**
     * @param $name
     * @return bool
     */
    protected function checkSort($name) {
        return Yii::$app->request->get('sort') && strpos(Yii::$app->request->get('sort'), $name) !== false;
    }


    public function exportFields()
    {
        return [
            'id',
            'firstname',
            'lastname',
            'created_at',
            'tripsTotal',
            'tripsMonthTotal',
            'tripsWeekTotal',

            'rating' => function($model) {
                /** @var $model \common\models\Driver */
                return $model->rating . '(' . $model->getReviews()->count() . ')';
            },
            'expiration_date',
            'lastActivity',
        ];
    }

}
