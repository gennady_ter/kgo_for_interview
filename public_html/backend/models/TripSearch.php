<?php

namespace backend\models;

use common\models\Trip;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * SettingSearch represents the model behind the search form of `backend\models\Setting`.
 */
class TripSearch extends Trip
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['route_id', 'driver_id'], 'required'],
            [['route_id', 'driver_id', 'status'], 'integer'],
            [['miles'], 'number'],
            [['start_at', 'created_at', 'wait_begin_at', 'wait_finish_at', 'finished_at'], 'safe'],
            ['status', 'in', 'range' => array_keys($this->statusLabels())]
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Trip::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);


        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'driver_id' => $this->driver_id,
        ]);

        return $dataProvider;
    }



    public function exportFields()
    {
        return [
            'id',
            'Pick up location' => function($model){
                /** @var $model \common\models\Trip */
                return $model->route->point_a;
            },
            'Destination' => function($model){
                /** @var $model \common\models\Trip */
                return $model->route->point_b;
            },
            'statusName',
            'Money Earned' => function($model) {
                /** @var $model \common\models\Trip */
                return $model->countMoney('driver');
            },
        ];
    }
}
