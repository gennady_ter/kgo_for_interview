<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Route;

/**
 * RouteSearch represents the model behind the search form of `common\models\Route`.
 */
class RouteSearch extends Route
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'group_id', 'child_id', 'driver_id', 'status'], 'integer'],
            [['point_a', 'point_b', 'is_roundtrip', 'group_id', 'date_start', 'date_end', 'days_of_week', 'time', 'created_at', 'updated_at'], 'safe'],
            [['nanny_hours', 'wait_hours'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Route::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'group_id' => $this->group_id,
            'child_id' => $this->child_id,
            'driver_id' => $this->driver_id,
            'nanny_hours' => $this->nanny_hours,
            'wait_hours' => $this->wait_hours,
            'date_start' => $this->date_start,
            'date_end' => $this->date_end,
            'time' => $this->time,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'point_a', $this->point_a])
            ->andFilterWhere(['like', 'point_b', $this->point_b])
            ->andFilterWhere(['like', 'is_roundtrip', $this->is_roundtrip])
            ->andFilterWhere(['like', 'group_id', $this->group_id])
            ->andFilterWhere(['like', 'days_of_week', $this->days_of_week]);

        return $dataProvider;
    }




    public function exportFields()
    {
        return [
            'id',
            'driver_id',
            'Client(s)' => function($model){
                /** @var $model \common\models\Route */
                return $model->group_id ? 'Group '.$model->group_id : 'Family '.$model->child->family->lastname;
            },
            'point_a',
            'time',
            'point_b',
            'date_end',
            'status' => function($model){
                /** @var $model \common\models\Route */
                return $model->getStatusName();
            }
        ];
    }
}
