<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Invoice;

/**
 * InvoiceSearch represents the model behind the search form of `common\models\Invoice`.
 */
class InvoiceSearch extends Invoice
{

    public function attributes()
    {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), [
            'trip.finished_at', 'trip.driver_id', 'driversBalance', 'familyId', 'createdPeriod'
        ]);
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'object_id', 'type'], 'integer'],
            [['amount'], 'number'],
            [['created_at', 'trip.finished_at', 'trip.driver_id', 'familyId', 'driversBalance', 'createdPeriod'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        // add related fields to searchable attributes
        return array_merge(parent::attributeLabels(), [
            'driversBalance' => 'Driver\'s Balance',
            'familyId' => 'Family ID',
            'createdPeriod' => 'Created Period',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function periodLabels() {
        return [7 => 'Week', 31 => 'Month', 365 => 'Year'];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Invoice::find()->joinWith(['trip', 'user']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $dataProvider->setSort([
            'attributes' => [
                'id',
                'trip.finished_at',
                'trip.driver_id',
                'amount',
                'created_at',
                'object_id',
                'user_id',
                'familyId' => [
                    'asc' => [],
                    'desc' => []
                ],
                'driversBalance' => [
                    'asc' => ['amount' => SORT_ASC],
                    'desc' => ['amount' => SORT_DESC]
                ],
            ]
        ]);

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'object_id' => $this->object_id,
            'invoice.type' => $this->type,
            'amount' => $this->amount,
            'trip.finished_at' => $this->getAttribute('trip.finished_at'),
            'trip.driver_id' => $this->getAttribute('trip.driver_id'),
            'familyId' => $this->user_id,
            'driversBalance' => $this->driversBalance,
        ]);

        if($this->created_at) {
            $query->andFilterWhere(['>', 'invoice.created_at', date('Y-m-d H:i:s', strtotime('-'.$this->created_at.' days'))]);
        }

        return $dataProvider;
    }

    /**
     * @param $name
     * @return bool
     */
    protected function checkSort($name) {
        return Yii::$app->request->get('sort') && strpos(Yii::$app->request->get('sort'), $name) !== false;
    }

    /**
     * @return array
     */
    public function exportFields()
    {
        $url = Yii::$app->request->referrer;
        if(strpos($url, 'type=1')) {
            $this->type = 1;
            return [
                'id',
                'created_at',
                'Driver ID' => function($model){
                    /** @var $model Invoice */
                    return $model->object_id;
                },
                'amount' => function($model) {
                    /** @var $model \common\models\Invoice */
                    return '$'.$model->amount;
                },
                'Driver\'s Balance' => function($model) {
                    /** @var $model \common\models\Invoice */
                    return '$'.$model->amount*0.8;
                },
            ];
        } elseif(strpos($url, 'type=2')) {
            $this->type = 2;
            return [
                'id',
                'user_id',
                'Trip ID' => function($model){
                    /** @var $model Invoice */
                    return $model->object_id;
                },
                'amount' => function($model) {
                    /** @var $model \common\models\Invoice */
                    return '$'.$model->amount;
                },
                'created_at'
            ];
        } else {
            return [
                'id',
                'Trip finished' => function($model){
                    /** @var $model Invoice */
                    return $model->trip->finished_at;
                },
                'Driver ID' => function($model){
                    /** @var $model Invoice */
                    return $model->trip->driver->id;
                },
                'Family ID' => function($model) {
                    /** @var $model Invoice */
                    return $model->user->family ? $model->user->family->id : 'none';
                },
                'amount' => function($model) {
                    /** @var $model \common\models\Invoice */
                    return '$'.$model->amount;
                },
                'Driver\'s Balance' => function($model) {
                    /** @var $model \common\models\Invoice */
                    return '$'.$model->amount*0.8;
                },
                'created_at',
            ];
        }

    }
}
