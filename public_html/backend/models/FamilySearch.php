<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Family;

/**
 * FamilySearch represents the model behind the search form of `common\models\Family`.
 */
class FamilySearch extends Family
{

    protected $query;

    public function attributes()
    {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), [
            'user.firstname', 'user.lastname', 'user.rating', 'moneySpent'
        ]);
    }


    public function attributeLabels()
    {
        // add related fields to searchable attributes
        return array_merge(parent::attributeLabels(), [
            'tripsTotal' => 'Total trips',
            'tripsMonthTotal' => 'Trips for a month',
            'tripsWeekTotal' => 'Trips for a week',
            'lastActivity' => 'Last activity',
        ]);
    }
    /**
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['lastname', 'description', 'banking_data', 'created_at', 'update_at', 'moneySpent', 'tripsTotal', 'tripsMonthTotal', 'tripsWeekTotal',], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->query = Family::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $this->query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $this->query->where('0=1');
            return $dataProvider;
        }

        $dataProvider->setSort([
            'attributes' => [
                'id',
                'lastname',
                'created_at',
                'tripsTotal' => [
                    'asc' => [],
                    'desc' => []
                ],
                'tripsMonthTotal' => [
                    'asc' => [],
                    'desc' => []
                ],
                'tripsWeekTotal' => [
                    'asc' => [],
                    'desc' => []
                ],
                'lastActivity',
                'moneySpent' => [
                    'asc' => [],
                    'desc' => []
                ],
            ]
        ]);


//        set calculable sorting
//        if($this->checkSort('lastActivity')) {
//            $this->query->innerJoin('trip', 'driver.id = trip.driver_id');
//        }

        // grid filtering conditions
        $this->query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'update_at' => $this->update_at,
        ]);


        $this->tripsQuery('tripsTotal');
        $this->tripsQuery('tripsMonthTotal');
        $this->tripsQuery('tripsWeekTotal');
        $this->tripsQuery('moneySpent');

        $this->query->andFilterWhere(['like', 'lastname', $this->lastname]);

        $sql = $this->query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;

        return $dataProvider;
    }

    protected function tripsQuery($attribute) {
        if($this->$attribute || $this->checkSort($attribute)) {
            $allTrips = [];
            foreach (Family::find()->all() as $item) {
                /** @var Family $item */
//                if (($this->$attribute == 'active' && $item->$attribute > 0) || $item->$attribute == $this->$attribute) {
                    $allTrips[$item->id] = $item->$attribute;
//                }
            }
//            sorting
            if($this->checkSort($attribute)) {
                if(Yii::$app->request->get('sort') == $attribute) {
                    asort($allTrips);
                } else {
                    arsort($allTrips);
                }
                $allTrips = array_keys($allTrips);
                $this->query->orderBy(new \yii\db\Expression('FIELD (id, ' . implode(',', $allTrips ).')'));
            }

            $this->query->andWhere(['in', 'id', $allTrips]);

        }
    }


    /**
     * @param $name
     * @return bool
     */
    protected function checkSort($name) {
        return Yii::$app->request->get('sort') && strpos(Yii::$app->request->get('sort'), $name) !== false;
    }



    public function exportFields()
    {
        return [
            'id',
            'lastname',
            'created_at',
            'tripsTotal',
            'tripsMonthTotal',
            'tripsWeekTotal',
            'moneySpent' => function($model) {
                /** @var $model \common\models\Family */
                return '$'.$model->moneySpent;
            }
        ];
    }
}
