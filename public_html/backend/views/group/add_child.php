<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $groupChildren common\models\GroupChildren */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="group-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($groupChildren, 'child_id')->dropDownList(\common\models\Child::getList()) ?>

    <div class="form-group">
        <?= Html::submitButton('Add child', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
