<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Group */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="group-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>


    <div class="row">
        <div class="col-sm-2"><label>Created:</label><br><?= $model->created_at ?></div>
        <div class="col-sm-2"><label>Updated:</label><br><?= $model->updated_at ?></div>
    </div>
    <hr>
    <div class="panel panel-default">
        <div class="panel-heading">Children in group</div>
        <div class="panel-body">
            <div class="row">
                <?php foreach ($model->children as $child) { ?>
                    <div class="col-sm-3">
                        <a href="/admin/child/view?id=<?= $child->id ?>"><?= $child->fullname ?></a>
                    </div>
                <?php } ?>
            </div>
            <hr>
            <div class="text-center">
                <a href="/admin/group/add-child?id=<?= $model->id ?>" class="btn btn-success">Add child</a>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Route</div>
        <div class="panel-body">
            <div class="row">
                <?php foreach ($model->routes as $route) { ?>
                    <div class="col-sm-3">
                        <a href="/admin/route/view?id=<?= $route->id ?>"><?= $route->fullname ?></a>
                    </div>
                <?php } ?>
            </div>
            <hr>
            <div class="text-center">
                <a href="/admin/route/create?groupId=<?= $model->id ?>" class="btn btn-success">Add route</a>
            </div>
        </div>
    </div>


</div>
