<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $child common\models\Child */

$this->title = $child->name;
$this->params['breadcrumbs'][] = ['label' => 'Children', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="child-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $child->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $child->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    
    <div class="row">
        <div class="col-sm-4">
            <div class="panel panel-primary">
                <div class="panel-heading"><?= $child->name ?></div>
                <div class="panel-body">
                    <?= \backend\components\PhotoHelper::render($child, 'photo') ?>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="panel panel-primary">
                <div class="panel-heading">Child info</div>
                <div class="panel-body">
                    <?= DetailView::widget([
                        'model' => $child,
                        'attributes' => [
                            'age',
                            'school_district',
                            'school_name',
                            'occupations',
                            'created_at',
                            'updated_at',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="panel panel-default">
                <div class="panel-heading">Additional info</div>
                <div class="panel-body">
                    <p>
                        <label>Family:</label>
                        <a href="/family/view?id=<?= $child->family_id ?>"><?= $child->family->lastname ?></a>
                    </p>
                    <hr>
                    <label>Driver:</label>
                    <?php if($child->driver_id) { ?>
                        <a href="/admin/driver/view?id=<?= $child->driver_id?>">
                            <img src="<?= $child->driver->photo?>" alt="" class="img-responsive">
                        </a>
                        <a href="/admin/driver/view?id=<?= $child->driver_id ?>">
                            <?= $child->driver->fullname ?>
                        </a>
                    <?php } ?>
                    <hr>

                </div>
            </div>
        </div>

    </div>

</div>
