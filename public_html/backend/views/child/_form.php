<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Child */
/* @var $form yii\widgets\ActiveForm */
$drivers = \common\models\Driver::getList();
?>

<div class="child-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'driver_id')->dropDownList(\common\models\Driver::getList()) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'age')->textInput() ?>

    <?= $form->field($model, 'photo')->fileInput() ?>

    <?= $form->field($model, 'school_district')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'school_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'occupations')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
