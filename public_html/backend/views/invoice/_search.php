<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\InvoiceSearch */
/* @var $form yii\widgets\ActiveForm */
/* @var $type integer */
?>

<div class="group-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index?type='.$type],
        'method' => 'get',
    ]); ?>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'object_id')->label('Driver ID') ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'createdPeriod')->dropDownList(['Week', 'Month', 'Year']) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
