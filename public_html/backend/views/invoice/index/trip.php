<?php
use common\models\Invoice;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\InvoiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $statistics int */

$this->title = 'Invoices';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="invoice-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <ul class="nav nav-tabs nav-justified">
        <li class="active"><a href="/admin/invoice/index">Trips</a></li>
        <li><a href="/admin/invoice/index?type=<?= Invoice::TYPE_DRIVER ?>">Driver Earnings</a></li>
        <li><a href="/admin/invoice/index?type=<?= Invoice::TYPE_PENALTY ?>">Penalties</a></li>
    </ul>
    <br>

    <?php Pjax::begin(); ?>

    <?php
    echo \phpnt\exportFile\ExportFile::widget([
        'model'             => '\backend\models\InvoiceSearch',
        'title'             => 'Routes',
        'queryParams'       => ['InvoiceSearch' => $searchModel->getAttributes()],
        'getAll'            => true,
        'buttonClass'       => 'btn btn-primary',                   // класс кнопки
        'blockClass'        => 'text-right',
        'blockStyle'        => 'padding: 0;',  // стиль блока в котором кнопка

        // экспорт в следующие файлы (true - разрешить, false - запретить)
        'xls'               => false,
        'csv'               => true,
        'word'              => false,
        'html'              => false,
        'pdf'               => false,

        'csvButtonName'     => 'Export <span class="badge badge-light">'.$statistics.'%</span>',
    ]) ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'trip.finished_at',
            'trip.driver_id',
            'familyId',

            [
                'attribute' => 'amount',
                'value' => function($model) {
                    /** @var $model \common\models\Invoice */
                    return '$'.$model->amount;
                }
            ],
            'driversBalance',
            [
                'attribute' => 'created_at',
                'filter' => $searchModel->periodLabels()
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
