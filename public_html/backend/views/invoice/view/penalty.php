<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $invoice common\models\Invoice */

$this->title = $invoice->id;
$this->params['breadcrumbs'][] = ['label' => 'Invoices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="invoice-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $invoice->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $invoice->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $invoice,
        'attributes' => [
            'id',
            'user_id',
            'object_id',
            [
                'attribute' => 'amount',
                'value' => function($model) {
                    /** @var $model \common\models\Invoice */
                    return '$'.$model->amount;
                }
            ],
            'created_at',
        ],
    ]) ?>

</div>
