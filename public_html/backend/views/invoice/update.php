<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $invoice common\models\Invoice */
/* @var $_params_ array */

$this->title = 'Update Invoice: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Invoices', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $invoice->id, 'url' => ['view', 'id' => $invoice->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="invoice-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', $_params_) ?>

</div>
