<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\FamilySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $statistics int */

$this->title = 'Families';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="family-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Family', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php
    echo \phpnt\exportFile\ExportFile::widget([
        'model'             => '\backend\models\FamilySearch',
        'title'             => 'Families',
        'queryParams'       => Yii::$app->request->queryParams,
        'getAll'            => true,
        'buttonClass'       => 'btn btn-primary',                   // класс кнопки
        'blockClass'        => 'text-right',
        'blockStyle'        => 'padding: 0;',  // стиль блока в котором кнопка

        // экспорт в следующие файлы (true - разрешить, false - запретить)
        'xls'               => false,
        'csv'               => true,
        'word'              => false,
        'html'              => false,
        'pdf'               => false,

        'csvButtonName'     => 'Export <span class="badge badge-light">'.$statistics.'%</span>',
    ]) ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'lastname',
            'created_at',
            [
                'attribute' => 'tripsTotal',
                'filter' => Html::activeCheckbox($searchModel, 'tripsTotal', ['value' => 'active', 'label' => 'Active'])
            ],
            [
                'attribute' => 'tripsMonthTotal',
                'filter' => Html::activeCheckbox($searchModel, 'tripsMonthTotal', ['value' => 'active', 'label' => 'Active'])
            ],
            [
                'attribute' => 'tripsWeekTotal',
                'filter' => Html::activeCheckbox($searchModel, 'tripsWeekTotal', ['value' => 'active', 'label' => 'Active'])
            ],
            [
                'attribute' => 'moneySpent',
                'value' => function($model) {
                    /** @var $model \common\models\Family */
                    return '$'.$model->moneySpent;
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
