<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $family common\models\Family */

$this->title = $family->lastname;
$this->params['breadcrumbs'][] = ['label' => 'Families', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="family-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $family->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $family->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]); ?>
    </p>

    <div class="row">
        <div class="col-sm-6"><p class="description"><?= $family->description ?></p></div>
        <div class="col-sm-6 text-right"><label>Banking data:</label> <?= $family->banking_data ?></div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading">Children</div>
                <div class="panel-body">
                    <div class="row">
                        <?php foreach($family->children as $child) { ?>
                            <div class="col-sm-6">
                                <?php if($child->photo) { ?>
                                    <a href="/admin/child/view?id=<?= $child->id ?>">
                                        <img src="<?= $child->photo?>" alt="" class="img-thumbnail">
                                    </a>
                                <?php } ?>
                                <a href="/admin/child/view?id=<?= $child->id ?>"><?= $child->name ?></a>
                            </div>
                        <?php } ?>
                    </div>
                    <hr>
                    <div class="text-center">
                        <a href="/admin/child/create?familyId=<?= $family->id ?>" class="btn btn-primary">Add child</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading">Family members</div>
                <div class="panel-body">
                    <?php foreach($family->familyMembers as $member) { ?>
                        <?php if($member->user->photo) { ?>
                            <a href="/admin/member/view?id=<?= $member->id ?>">
                                <img src="<?= $member->user->photo?>" alt="" class="img-responsive">
                            </a>
                        <?php } ?>
                        <a href="/admin/member/view?id=<?= $member->id ?>"><?= $member->user->firstname.' '.$member->user->lastname ?></a>
                        <hr>
                    <?php } ?>
                    <div class="text-center">
                        <a href="/admin/member/create?familyId=<?= $family->id ?>" class="btn btn-primary">Add family member</a>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="panel panel-success">
        <div class="panel-heading">Trips</div>
        <div class="panel-body">

            <table id="w0" class="table table-striped table-bordered detail-view">
                <thead>
                <tr>
                    <th>Client(s)</th>
                    <th>From</th>
                    <th>To</th>
                    <th>Status</th>
                    <th>Money earned</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($family->trips as $trip) { ?>
                    <tr>
                        <td>
                            <?php if($trip->route->group_id) { ?>
                                <a href="/admin/group/view?id=<?= $trip->route->group_id ?>" class="btn btn-sm btn-default">
                                    <?= $trip->route->object->name ?>
                                </a>
                            <?php } ?>
                        </td>
                        <td><?= $trip->route->point_a ?></td>
                        <td><?= $trip->route->point_b ?></td>
                        <td>
                            <span class="badge badge-<?= $trip->status == 0 ? 'primary' : ($trip->status == 1 ? 'success' : 'error') ?>">
                                <?= $trip->statusName ?>
                            </span>
                        </td>
                        <td>$<?= $trip->countMoney('driver') ?></td>
                        <td class="text-right"><a href="/admin/route/view?id=<?= $trip->route_id ?>" class="btn btn-sm btn-primary">View route</a></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>


            <?php
            // todo: change family queryparams to get trips
            echo \phpnt\exportFile\ExportFile::widget([
                'model'             => '\backend\models\TripSearch',
                'title'             => 'Family\'s trips',
                'queryParams'       => Yii::$app->request->queryParams,
                'getAll'            => true,
                'buttonClass'       => 'btn btn-success',                   // класс кнопки
                'blockClass'        => 'text-right',
                'blockStyle'        => 'padding: 0;',  // стиль блока в котором кнопка

                // экспорт в следующие файлы (true - разрешить, false - запретить)
                'xls'               => false,
                'csv'               => true,
                'word'              => false,
                'html'              => false,
                'pdf'               => false,

                // шаблоны кнопок
                'xlsButtonName'     => 'MS Excel',
                'csvButtonName'     => 'Export trips',
                'wordButtonName'    => 'MS Word',
                'htmlButtonName'    => 'HTML',
                'pdfButtonName'     => 'PDF',
            ]) ?>

        </div>
    </div>


</div>
