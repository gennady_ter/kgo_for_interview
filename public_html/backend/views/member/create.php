<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $member common\models\FamilyMember */
/* @var $user common\models\User */
/* @var $_params_ array */

$this->title = 'Add Family Member';
$this->params['breadcrumbs'][] = ['label' => 'Drivers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="driver-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', $_params_) ?>

</div>
