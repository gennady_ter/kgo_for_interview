<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\Recommendation;

/* @var $this yii\web\View */
/* @var $member common\models\FamilyMember */

$this->title = $member->user->firstname.' '.$member->user->lastname;
$this->params['breadcrumbs'][] = ['label' => 'Family members', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="driver-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $member->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $member->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <div class="row">
        <div class="col-sm-4">
            <div class="panel panel-info">
                <div class="panel-heading">Main info</div>
                <div class="panel-body">
                    <?php if($member->photo) { ?>
                        <a href="<?= $member->photo?>" target="_blank">
                            <img src="<?= $member->photo?>" alt="" class="img-responsive">
                        </a>
                    <?php } else { ?>
                        <p class="alert alert-danger">No user's photo!</p>
                    <?php } ?>

                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="panel panel-default">
                <div class="panel-heading">Additional client's info</div>
                <div class="panel-body">

                    <?= DetailView::widget([
                        'model' => $member,
                        'attributes' => [
                            'firstname',
                            'lastname',
                            'username',
                            'email',
                            ['attribute' => 'status', 'value' => $member->user->statusName()],
                            'address',
                            'rating',
                            'role',
                            'occupation',
                            'description:ntext',
                            ['attribute' => 'created_at', 'value' => date('Y-m-d H:i:s', $member->user->created_at)],
                            ['attribute' => 'updated_at', 'value' => date('Y-m-d H:i:s', $member->user->created_at)],
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="panel panel-default">
                <div class="panel-heading">Family info</div>
                <div class="panel-body">
                    <h3><a href="/admin/family/view?id=<?= $member->family_id ?>"><?= $member->family->lastname ?></a></h3>
                    <label>Children:</label>
                    <?php foreach($member->family->children as $child) { ?>
                        <?php if($child->photo) { ?>
                            <a href="<?= $child->photo?>" target="_blank">
                                <img src="<?= $child->photo?>" alt="" class="img-thumbnail">
                            </a>
                        <?php } ?>
                        <a href="/admin/child/view?id=<?= $child->id ?>"><?= $child->name ?></a>
                        <hr>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
