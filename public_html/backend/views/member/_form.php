<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $member common\models\FamilyMember */
/* @var $user common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="driver-form">

    <?php $form = ActiveForm::begin(['id' => 'update-driver', 'options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($user, 'type')->hiddenInput()->label(false) ?>

    <div class="row">
        <div class="col-sm-8">
            <div class="panel">
                <div class="panel-heading">
                    User's data
                </div>
                <div class="panel-body">
                    <div class="col-sm-6">
                        <?= $form->field($user, 'firstname')->textInput() ?>
                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($user, 'lastname')->textInput() ?>
                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($user, 'username')->textInput() ?>
                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($user, 'email')->textInput() ?>
                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($user, 'address')->textInput() ?>
                    </div>
                    <?= $form->field($user, 'rating')->hiddenInput(['value' => 5])->label(false) ?>
                    <div class="col-sm-6">
                        <?= $form->field($user, 'status')->dropDownList($user->statuses()) ?>
                    </div>
                    <div class="col-sm-6">
                    <?php if($user->photo) {
                        echo Html::img($user->photo, ['class' => 'img-responsive']);
                    } ?>
                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($user, 'photo')->fileInput() ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="panel">
                <div class="panel-heading">Driver's data</div>
                <div class="panel-body">
                    <?= $form->field($member, 'role')->textInput() ?>
                    <?= $form->field($member, 'occupation')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($member, 'description')->textarea(['rows' => 4]) ?>
                </div>
            </div>
        </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
