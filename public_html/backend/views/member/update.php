<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $member common\models\FamilyMember */
/* @var $user common\models\User */
/* @var $_params_ array */

$this->title = 'Update Family Member: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Clients', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $member->user->firstname.' '.$member->user->lastname, 'url' => ['view', 'id' => $member->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="driver-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', $_params_) ?>

</div>
