<?php

use yii\helpers\Html;
use yii\grid\GridView;
use phpnt\exportFile\ExportFile;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\DriverSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $monthActive int */
/* @var $weekActive int */
/* @var $totalCount int */

$this->title = 'Drivers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="driver-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Driver', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php
    echo ExportFile::widget([
        'model'             => '\backend\models\DriverSearch',
        'title'             => 'Drivers index',
        'queryParams'       => Yii::$app->request->queryParams,
        'getAll'            => true,
        'buttonClass'       => 'btn btn-primary',                   // класс кнопки
        'blockClass'        => 'text-right',
        'blockStyle'        => 'padding: 0;',  // стиль блока в котором кнопка

        // экспорт в следующие файлы (true - разрешить, false - запретить)
        'xls'               => false,
        'csv'               => true,
        'word'              => false,
        'html'              => false,
        'pdf'               => false,

        // шаблоны кнопок
        'xlsButtonName'     => 'MS Excel',
        'csvButtonName'     => 'Export <span class="badge badge-light">'.$statistics.'%</span>',
        'wordButtonName'    => 'MS Word',
        'htmlButtonName'    => 'HTML',
        'pdfButtonName'     => 'PDF',
    ]) ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'firstname',
            'lastname',
            'created_at',
            [
                'attribute' => 'tripsTotal',
                'filter' => Html::activeCheckbox($searchModel, 'tripsTotal', ['value' => 'active', 'label' => 'Active'])
            ],
            [
                'attribute' => 'tripsMonthTotal',
                'filter' => Html::activeCheckbox($searchModel, 'tripsMonthTotal', ['value' => 'active', 'label' => 'Active'])
            ],
            [
                'attribute' => 'tripsWeekTotal',
                'filter' => Html::activeCheckbox($searchModel, 'tripsWeekTotal', ['value' => 'active', 'label' => 'Active'])
            ],
            [
                'attribute' => 'rating',
                'value' => function($model){
                    /** @var $model \common\models\Driver */
                    return $model->rating.'('.$model->getReviews()->count().')';
                }
            ],
            'expiration_date',
            'lastActivity',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
