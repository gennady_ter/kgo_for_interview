<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $driver common\models\Driver */
/* @var $_params_ array */

$this->title = 'Update Driver: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Drivers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $driver->id, 'url' => ['view', 'id' => $driver->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="driver-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_forms/driver', $_params_) ?>

</div>
