<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $driver common\models\Driver */
/* @var $user common\models\User */
/* @var $_params_ array */

$this->title = 'Create Driver';
$this->params['breadcrumbs'][] = ['label' => 'Drivers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="driver-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_forms/driver', $_params_) ?>

</div>
