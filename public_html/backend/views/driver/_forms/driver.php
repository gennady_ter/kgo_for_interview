<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $driver common\models\Driver */
/* @var $user common\models\User */
/* @var $bankingData common\models\BankingData */
/* @var $car common\models\Car */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="driver-form">

    <?php $form = ActiveForm::begin(['id' => 'update-driver', 'options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($user, 'type')->hiddenInput()->label(false) ?>

    <div class="row">
        <div class="col-sm-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    User's data
                </div>
                <div class="panel-body">
                    <?= $form->field($user, 'firstname')->textInput() ?>

                    <?= $form->field($user, 'lastname')->textInput() ?>

                    <?= $form->field($user, 'username')->textInput() ?>

                    <?= $form->field($user, 'email')->textInput() ?>

                    <?= $form->field($user, 'address')->textInput() ?>

                    <?= $form->field($user, 'rating')->radioList([1=>1,2=>2,3=>3,4=>4,5=>5]) ?>

                    <?= $form->field($user, 'status')->dropDownList($user->statuses()) ?>

                    <?php if($user->photo) {
                        echo Html::img($user->photo, ['class' => 'img-responsive']);
                    } ?>
                    <?= $form->field($user, 'photo')->fileInput() ?>
                </div>
            </div>
            <div class="panel panel-success">
                <div class="panel-heading">Banking data</div>
                <div class="panel-body">
                    <?= $form->field($bankingData, 'cardnumber')->textInput() ?>
                    <?= $form->field($bankingData, 'expiration_date')->textInput() ?>
                    <?= $form->field($bankingData, 'cvv')->textInput() ?>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="panel panel-default">
                <div class="panel-heading">Driver's data</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <?= $form->field($driver, 'birthdate')->textInput() ?>
                        </div>
                        <div class="col-sm-6">
                            <?= $form->field($driver, 'ethnicity')->dropDownList($driver->ethnicities()) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <?= $form->field($driver, 'sex')->dropDownList($driver->sexValues()) ?>
                        </div>
                        <div class="col-sm-6">
                            <?= $form->field($driver, 'family_status')->dropDownList($driver->familyStatuses()) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <?= $form->field($driver, 'current_job')->textInput() ?>
                        </div>
                        <div class="col-sm-6">
                            <?= $form->field($driver, 'experience')->textInput() ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <?= $form->field($driver, 'is_experienced_mom')->checkbox() ?>
                        </div>
                        <div class="col-sm-4">
                            <?= $form->field($driver, 'available_from')->textInput() ?>
                        </div>
                        <div class="col-sm-4">
                            <?= $form->field($driver, 'available_to')->textInput() ?>
                        </div>
                    </div>
                    <?= $form->field($driver, 'description')->textarea(['rows' => 4]) ?>
                    <?= $form->field($driver, 'education')->textarea(['rows' => 4]) ?>

                    <hr/>
                    <h3>Documents</h3>

                    <div class="row">
                        <div class="col-sm-6">
                            <?php if($driver->resume) {
                                echo Html::img($driver->resume, ['class' => 'img-responsive']);
                            } ?>
                            <?= $form->field($driver, 'resume')->fileInput() ?>
                        </div>
                        <div class="col-sm-6">
                            <?php if($driver->drivers_license) {
                                echo Html::img($driver->drivers_license, ['class' => 'img-responsive']);
                            } ?>
                            <?= $form->field($driver, 'drivers_license')->fileInput() ?>
                            <?= $form->field($driver, 'expiration_date')->textInput() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-warning">
        <div class="panel-heading">Info about car</div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-3">
                    <?= $form->field($car, 'mark')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-sm-3">
                    <?= $form->field($car, 'model')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-sm-3">
                    <?= $form->field($car, 'year')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-sm-3">
                    <?= $form->field($car, 'efficiency')->textInput() ?>
                </div>
                <div class="col-sm-3">
                    <?= $form->field($car, 'seats')->textInput() ?>
                </div>
                <div class="col-sm-3">
                    <?= $form->field($car, 'number')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-sm-3">
                    <?php if($car->insurance) {
                        echo Html::img($car->insurance, ['class' => 'img-responsive']);
                    } ?>
                    <?= $form->field($car, 'insurance')->fileInput() ?>
                </div>
                <div class="col-sm-3">
                    <?php if($car->registration) {
                        echo Html::img($car->registration, ['class' => 'img-responsive']);
                    } ?>
                    <?= $form->field($car, 'registration')->fileInput() ?>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
