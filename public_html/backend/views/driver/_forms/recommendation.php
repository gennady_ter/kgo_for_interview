<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $recommendation \common\models\Recommendation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="job-history-form">

    <?php $form = ActiveForm::begin(['id' => 'update-driver', 'options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="row">
        <h4>Recommendation</h4>
        <div class="col-sm-6">
            <?= $form->field($recommendation, 'firstname')->textInput() ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($recommendation, 'lastname')->textInput() ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($recommendation, 'phone_number')->textInput() ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($recommendation, 'email')->textInput() ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($recommendation, 'information')->textInput() ?>
        </div>
        <?php if($recommendation->type == $recommendation::TYPE_WORK) { ?>
            <div class="col-sm-6">
                <?= $form->field($recommendation, 'additional')->textInput() ?>
            </div>
        <?php } ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
