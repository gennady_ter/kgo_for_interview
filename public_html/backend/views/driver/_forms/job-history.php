<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $jobHistory \common\models\JobHistory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="job-history-form">

    <?php $form = ActiveForm::begin(['id' => 'update-driver', 'options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($jobHistory, 'information')->textarea() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
