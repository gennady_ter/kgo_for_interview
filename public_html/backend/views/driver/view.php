<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\Recommendation;

/* @var $this yii\web\View */
/* @var $driver common\models\Driver */
/* @var $user common\models\User */
/* @var $car common\models\Car */

$this->title = $driver->id;
$this->params['breadcrumbs'][] = ['label' => 'Drivers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="driver-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $driver->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $driver->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <div class="row">
        <div class="col-sm-4">
            <div class="panel panel-info">
                <div class="panel-heading">Main info</div>
                <div class="panel-body">
                    <?php if($user->photo) { ?>
                        <a href="<?= $user->photo?>" target="_blank">
                            <img src="<?= $user->photo?>" alt="" class="img-responsive">
                        </a>
                    <?php } else { ?>
                        <p class="alert alert-danger">No user's photo!</p>
                    <?php } ?>
                    <hr>
                    <?= DetailView::widget([
                        'model' => $user,
                        'attributes' => [
                            'id',
                            'firstname',
                            'lastname',
                            'username',
                            'email',
                            ['attribute' => 'status', 'value' => $user->statusName()],
                            'address',
                            'rating',
                            [
                                'label' => 'Number of reviews',
                                'value' => function($model){
                                    /** @var $model \common\models\Driver */
                                    return $model->getReviews()->count();
                                }
                            ],
                            ['attribute' => 'created_at', 'value' => date('Y-m-d H:i:s', $user->created_at)],
                            ['attribute' => 'updated_at', 'value' => date('Y-m-d H:i:s', $user->created_at)],
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="panel panel-default">
                <div class="panel-heading">Additional driver's info</div>
                <div class="panel-body">
                    <?php if($driver->drivers_license) { ?>
                        <a href="<?= $driver->drivers_license ?>" target="_blank">
                            <img src="<?= $driver->drivers_license ?>" alt="" class="img-responsive">
                        </a>
                    <?php } else { ?>
                        <p class="alert alert-danger">No driver's license!</p>
                    <?php } ?>
                    <hr>
                    <?= DetailView::widget([
                        'model' => $driver,
                        'attributes' => [
                            'expiration_date',
                            'description:ntext',
                            'birthdate',
                            'ethnicity',
                            ['attribute' => 'sex', 'value' => $driver->sexName()],
                            ['attribute' => 'family_status', 'value' => $driver->familyStatusName()],
                            'education:ntext',
                            'current_job',
                            'experience',
                            ['attribute' => 'is_experienced_mom', 'value' => $driver->is_experienced_mom ? 'Yes' : 'No'],
                            [
                                'attribute' => 'resume',
                                'format' => 'raw',
                                'value' => function ($model) {
                                    if($model->resume) {
                                        return Html::a('File', $model->resume, ['target' => '_blank']);
                                    } else {
                                        return Html::tag('p', 'None!', ['class' => 'alert alert-danger']);
                                    }
                                },
                            ],
                            'available_from',
                            'available_to',
                            'created_at',
                            'updated_at',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="panel panel-default">
                <div class="panel-heading">Car info</div>
                <div class="panel-body">
                    <?php if($car->insurance) { ?>
                        <label>Insurance:</label>
                        <a href="<?= $car->insurance ?>" target="_blank">
                            <img src="<?= $car->insurance ?>" alt="" class="img-responsive">
                        </a>
                    <?php } else { ?>
                        <p class="alert alert-danger">No car insurance!</p>
                    <?php } ?>
                    <hr>
                    <?php if($car->registration) { ?>
                        <label>Registration:</label>
                        <a href="<?= $car->registration ?>" target="_blank">
                            <img src="<?= $car->registration ?>" alt="" class="img-responsive">
                        </a>
                    <?php } else { ?>
                        <p class="alert alert-danger">No car registration!</p>
                    <?php } ?>
                    <hr>
                    <?= DetailView::widget([
                        'model' => $car,
                        'attributes' => [
                            'mark',
                            'model',
                            'year',
                            'efficiency',
                            'seats',
                            'number',
                            'created_at',
                            'updated_at',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-5">
            <div class="panel panel-default">
                <div class="panel-heading">Work Experience</div>
                <div class="panel-body">
                    <?php foreach ($driver->jobHistories as $jobHistory) { ?>
                        <blockquote class="blockquote">
                            <p class="mb-0"><?= $jobHistory->information ?></p>
                        </blockquote>
                    <?php } ?>
                    <div class="text-center">
                        <a href="create-job-history?id=<?= $driver->id ?>" class="btn btn-primary">Add work experience</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-7">
            <div class="panel panel-default">
                <div class="panel-heading">Recommendations</div>
                <div class="panel-body">
                    <?php foreach ($driver->recommendations as $recommendation) { ?>
                        <blockquote class="blockquote">
                            <p class="mb-0">

                                Phone number: <?= $recommendation->phone_number ?><br>
                                Email: <?= $recommendation->email ?><br>
                            </p>
                            <footer class="blockquote-footer">
                                <cite title="Source Title">
                                    <?= $recommendation->additional.' '.$recommendation->information.' '.$recommendation->firstname.' '.$recommendation->lastname ?>
                                </cite>
                                <?php if($recommendation->type == $recommendation::TYPE_PERSONAL) { ?>
                                    <span class="badge">Personal</span>
                                <?php } ?>
                            </footer>
                        </blockquote>
                    <?php } ?>
                    <div class="btn-group row">
                        <div class="col-sm-6 text-left">
                            <a href="create-recommendation?id=<?= $driver->id ?>&type=<?= Recommendation::TYPE_PERSONAL ?>" class="btn btn-primary">Create personal recommendation</a>
                        </div>
                        <div class="col-sm-6 text-right">
                            <a href="create-recommendation?id=<?= $driver->id ?>&type=<?= Recommendation::TYPE_WORK ?>" class="btn btn-primary">Create work recommendation</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-success">
        <div class="panel-heading">Trips</div>
        <div class="panel-body">

            <table id="w0" class="table table-striped table-bordered detail-view">
                <thead>
                    <tr>
                        <th>Client(s)</th>
                        <th>From</th>
                        <th>To</th>
                        <th>Status</th>
                        <th>Money earned</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($driver->trips as $trip) { ?>
                    <tr>
                        <td>
                            <?php if($trip->route->group_id) { ?>
                                <a href="/admin/group/view?id=<?= $trip->route->group_id ?>" class="btn btn-sm btn-default">
                                    <?= $trip->route->object->name ?>
                                </a>
                            <?php } ?>
                        </td>
                        <td><?= $trip->route->point_a ?></td>
                        <td><?= $trip->route->point_b ?></td>
                        <td>
                            <span class="badge badge-<?= $trip->status == 0 ? 'primary' : ($trip->status == 1 ? 'success' : 'error') ?>">
                                <?= $trip->statusName ?>
                            </span>
                        </td>
                        <td>$<?= $trip->countMoney('driver') ?></td>
                        <td class="text-right"><a href="/admin/route/view?id=<?= $trip->route_id ?>" class="btn btn-sm btn-primary">View route</a></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>

            <?php
            echo \phpnt\exportFile\ExportFile::widget([
                'model'             => '\backend\models\TripSearch',
                'title'             => 'Driver\'s trips',
                'queryParams'       => ['TripSearch' => ['driver_id' => $driver->id]],
                'getAll'            => true,
                'buttonClass'       => 'btn btn-success',                   // класс кнопки
                'blockClass'        => 'text-right',
                'blockStyle'        => 'padding: 0;',  // стиль блока в котором кнопка

                // экспорт в следующие файлы (true - разрешить, false - запретить)
                'xls'               => false,
                'csv'               => true,
                'word'              => false,
                'html'              => false,
                'pdf'               => false,

                // шаблоны кнопок
                'xlsButtonName'     => 'MS Excel',
                'csvButtonName'     => 'Export trips',
                'wordButtonName'    => 'MS Word',
                'htmlButtonName'    => 'HTML',
                'pdfButtonName'     => 'PDF',
            ]) ?>
        </div>
    </div>

</div>
