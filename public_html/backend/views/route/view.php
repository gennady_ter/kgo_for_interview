<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\Invoice;

/* @var $this yii\web\View */
/* @var $route common\models\Route */

$this->title = $route->id;
$this->params['breadcrumbs'][] = ['label' => 'Routes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="route-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-sm-6">
            <?= Html::a('Update', ['update', 'id' => $route->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $route->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </div>
        <div class="col-sm-6 text-right">
            <a href="/admin/invoice/create?type=<?= Invoice::TYPE_PENALTY ?>&objectId=<?= $route->id ?>" class="btn btn-default">
                Create penalty invoice
            </a>
        </div>

    </div>
    <hr>

    <div class="row">
        <div class="col-sm-4">
            <div class="panel panel-primary">
                <div class="panel-heading">Main info</div>
                <div class="panel-body">
                    <?= DetailView::widget([
                        'model' => $route,
                        'attributes' => [
                            'id',
                            [
                                'label' => 'Passenger (s)',
                                'format' => 'html',
                                'value' => function($data) {
                                    if($data->group_id) {
                                        return Html::a($data->group->name, '/admin/group/view?id='.$data->group_id);
                                    } else {
                                        return Html::a($data->child->name, '/admin/child/view?id='.$data->child_id);
                                    }

                                }
                            ],
                            [
                                'label' => 'Family member',
                                'attribute' => 'family_member_id',
                                'value' => function($data) {
                                    /** @var $data \common\models\Route */
                                    return $data->familyMember->id;

                                }
                            ],
                            'point_a:ntext',
                            'point_b:ntext',
                            'is_roundtrip',
                            'nanny_hours',
                            'wait_hours',
                            'date_start',
                            'date_end',
                            [
                                'attribute' => 'days_of_week',
                                'value' => function($data) {
                                    $result = null;
                                    if(is_array($data->days_of_week)) {
                                        foreach ($data->days_of_week as $day) {
                                            $result .= $data->dayLabels($day).', ';
                                        }
                                    }
                                    return $result;
                                }
                            ],
                            'time',
                            'created_at',
                            'updated_at',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="panel panel-default">
                <div class="panel-heading">Trips</div>
                <div class="panel-body">
                    <?php foreach ($route->trips as $trip) { ?>
                        <?= DetailView::widget([
                            'model' => $trip,
                            'attributes' => [
                                'id',
                                'created_at',
                                'miles',
                                'wait_begin_at',
                                'wait_finish_at',
                                'finished_at',
                            ],
                        ]) ?>
                        <div class="text-right">
                            <a href="/admin/invoice/create?type=<?= Invoice::TYPE_TRIP ?>&objectId=<?= $trip->id ?>" class="btn btn-default">
                                Create trip invoice
                            </a>
                        </div>
                        <hr>
                    <?php } ?>
                    <div class="text-center">
                        <a href="/admin/route/create-trip?id=<?= $route->id ?>" class="btn btn-success">Add trip</a>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
