<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\RouteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $statistics int */

$this->title = 'Routes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="route-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Route', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php
    echo \phpnt\exportFile\ExportFile::widget([
        'model'             => '\backend\models\RouteSearch',
        'title'             => 'Routes',
        'queryParams'       => Yii::$app->request->queryParams,
        'getAll'            => true,
        'buttonClass'       => 'btn btn-primary',                   // класс кнопки
        'blockClass'        => 'text-right',
        'blockStyle'        => 'padding: 0;',  // стиль блока в котором кнопка

        // экспорт в следующие файлы (true - разрешить, false - запретить)
        'xls'               => false,
        'csv'               => true,
        'word'              => false,
        'html'              => false,
        'pdf'               => false,

        'csvButtonName'     => 'Export <span class="badge badge-light">'.$statistics.'%</span>',
    ]) ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'driver_id',
            [
                'label' => 'Client(s)',
                'format' => 'html',
                'value' => function($model) {
                    $options = ['class' => 'btn btn-sm btn-primary'];
                    /** @var $model \common\models\Route */
                    return $model->group_id ?
                        Html::a('View group', ['group/view', 'id' => $model->group_id], $options) :
                        Html::a($model->child->family->lastname, ['/family/view', 'id' => $model->child->family_id], $options);
                }
            ],
            'point_a:ntext',
            'time',
            'point_b:ntext',
            'date_end',
            [
                'attribute' => 'status',
                'filter' => $searchModel->statusLabels(),
                'value' => function($model){
                    /** @var $model \common\models\Route */
                    return $model->getStatusName();
                }
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
