<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $trip common\models\Trip */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Create Trip';
$this->params['breadcrumbs'][] = ['label' => 'Routes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Route', 'url' => ['view', 'id' => $trip->route_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trip-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="trip-form">

        <?php $form = ActiveForm::begin(); ?>

        <div class="row">
            <div class="col-sm-3">
                <?= $form->field($trip, 'miles')->textInput() ?>
            </div>
            <div class="col-sm-3">
                <?= $form->field($trip, 'wait_begin_at')->textInput() ?>
            </div>
            <div class="col-sm-3">
                <?= $form->field($trip, 'wait_finish_at')->textInput() ?>
            </div>
            <div class="col-sm-3">
                <?= $form->field($trip, 'finished_at')->textInput() ?>
            </div>
        </div>

        <div class="form-group text-center">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
