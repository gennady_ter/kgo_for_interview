<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Route */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="route-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'point_a')->textInput() ?>
            <?= $form->field($model, 'point_b')->textInput() ?>
            <?= $form->field($model, 'is_roundtrip')->checkbox() ?>
            <?= $form->field($model, 'nanny_hours')->textInput() ?>
            <?= $form->field($model, 'wait_hours')->textInput() ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'date_start')->textInput() ?>
            <?= $form->field($model, 'date_end')->textInput() ?>
            <?= $form->field($model, 'days_of_week')->checkboxList(['Sun', 'Mon', 'Tue', 'Wen', 'Thu', 'Fri', 'Sat']) ?>
            <?= $form->field($model, 'time')->textInput() ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
