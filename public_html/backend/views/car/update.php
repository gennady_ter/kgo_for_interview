<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $car common\models\Car */
/* @var $_params_ array */

$this->title = 'Update Car: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Cars', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $car->id, 'url' => ['view', 'id' => $car->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="car-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', $_params_) ?>

</div>
