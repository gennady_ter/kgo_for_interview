-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Июл 28 2018 г., 17:04
-- Версия сервера: 5.7.22-0ubuntu0.16.04.1
-- Версия PHP: 7.1.18-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `admin_kngo`
--

-- --------------------------------------------------------

--
-- Структура таблицы `api_config`
--

CREATE TABLE `api_config` (
  `id` int(11) NOT NULL,
  `controller` varchar(255) NOT NULL,
  `action` varchar(255) NOT NULL,
  `attributes` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `api_config`
--

INSERT INTO `api_config` (`id`, `controller`, `action`, `attributes`) VALUES
(1, 'api/family-member', 'index', 'id,user.photo,user.firstname,user.lastname'),
(2, 'api/family-member', 'view', 'id,role,description,occupation,,user.username,user.photo,user.firstname,user.lastname,user.address,user.bankingData,user.auth_key'),
(3, 'api/child', 'index', 'id,photo,name'),
(4, 'api/driver', 'choose', 'id,user.firstname,user.lastname,user.photo,user.rating,car.mark,car.model'),
(5, 'api/driver', 'search', 'id,user.firstname,user.lastname,user.photo,user.rating,car.mark,car.model'),
(6, 'api/driver', 'view', 'id,user.firstname,user.lastname,user.photo,user.rating,car.mark,car.model'),
(7, 'api/family-member', 'create', 'id,user.photo,user.firstname,user.lastname,user.auth_key'),
(8, 'api/trip', 'index', 'id,driver.user,driver.car,route.point_a,route.point_b,route.date_start,route.time,route.money,route.object.children');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `api_config`
--
ALTER TABLE `api_config`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `api_config`
--
ALTER TABLE `api_config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
